namespace Api

open System
open System.Configuration
open System.Globalization

module Utils = 
    open System.Drawing
    open System.Drawing.Drawing2D
    open System.Drawing.Imaging

    type Opt = Runtime.InteropServices.OptionalAttribute

    type Def = Runtime.InteropServices.DefaultParameterValueAttribute

    type Value<'a> = { Value : 'a; Text : string }

    type Page<'a> = { Total : int; Items : array<'a> }

    let host (uri: Uri) = Uri(uri.GetLeftPart(UriPartial.Authority))

    let inline (|?) (a: 'a option) b = if a.IsSome then a.Value else b

    let connection(name:string) = ConfigurationManager.ConnectionStrings.[name].ConnectionString

    let config(name:string) = ConfigurationManager.AppSettings.Get(name)

    let toOption value = if isNull(box value) then None else Some(value)

    let cstNow() = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "Eastern Standard Time")   
    
    let resizeImage width height (image: Image) = 
        let targetRectangle = Rectangle(0, 0, width, height)
        let targetImage = new Bitmap(width, height)
        targetImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        use g = Graphics.FromImage(targetImage)
        g.CompositingMode <- CompositingMode.SourceCopy
        g.CompositingQuality <- CompositingQuality.HighQuality
        g.InterpolationMode <- InterpolationMode.HighQualityBicubic
        g.SmoothingMode <- SmoothingMode.HighQuality
        g.PixelOffsetMode <- PixelOffsetMode.HighQuality

        use attributes = new ImageAttributes()
        attributes.SetWrapMode(WrapMode.TileFlipXY)
        g.DrawImage(image, targetRectangle, 0, 0, image.Width,image.Height, GraphicsUnit.Pixel, attributes)
        
        targetImage
    
    let inline source q = 
        <@query.Source (q)@>

    let inline where predicate source = 
        <@ query.Where(%source, %predicate) @>

    let inline whereIf predicate condition source = 
        if condition then <@ query.Where(%source, %predicate) @> else <@ %source @>

    let inline select projection source = 
        <@ query.Select(%source, %projection) @>

    let inline sortBy projection source = 
        <@ query.SortBy(%source, %projection) @>

    let inline thenBy projection source = 
        <@ query.ThenBy(%source, %projection) @>

    let inline sortByDesc projection source = 
        <@ query.SortByDescending(%source, %projection) @>

    let inline thenByDesc projection source = 
        <@ query.ThenByDescending(%source, %projection) @>

    let inline find predicate source = 
        <@ query.Find(%source, %predicate) @>

    let inline headOrDefault source = 
        <@ query.HeadOrDefault(%source) @>

    let inline exactlyOne source = 
        <@ query.ExactlyOne(%source) @>

    let inline exactlyOneOrDefault source = 
        <@ query.ExactlyOneOrDefault(%source) @>

    let inline count source =
        <@ query.Count(%source) @>

    let inline contains source key =
        <@ query.Contains(%source, key) @>

    let inline exist predicate source = 
        <@ query.Exists(%source, %predicate) @>

    let inline leftOuterJoin innerSource inner outer result outerSource =
        <@ query.LeftOuterJoin(%outerSource, %innerSource, %outer, %inner, %result) @>

    let inline join innerSource outer inner result outerSource =
        <@ query.Join(%outerSource, %innerSource, %outer, %inner, %result) @>

    let inline group keySelector source =
        <@ query.GroupBy(%source, %keySelector)@>

    let inline groupValBy keySelector resultSelector source =
        <@ query.GroupValBy(%source, %resultSelector, %keySelector) @>

    let inline take count source = 
        <@ query.Take(%source, count) @>

    let inline skip count source = 
        <@ query.Skip(%source, count) @>

    let toPage (page : int) (limit : int) q = 
        let items() = async { return (skip ((page - 1) * limit) >> take limit >> query.Run >> Seq.toArray) q }
        let total() = async { return (count >> query.Run) q }
        async { 
            let! items = items()
            let! total = total()
            return { Page.Total = total; Items = items }
        }
        |> Async.RunSynchronously
        
module Background = 
    open System.Web.Hosting
    
    let action f p = HostingEnvironment.QueueBackgroundWorkItem(Action<_>(fun _ -> f(p)))

module Railway =

    type Result<'TSuccess,'TFailure> = 
        | Success of 'TSuccess
        | Failure of 'TFailure
   
    let succeed x = 
        Success x
    
    let fail x = 
        Failure x 
   
    let either successFunc failureFunc twoTrackInput =
        match twoTrackInput with
        | Success s -> successFunc s
        | Failure f -> failureFunc f
   
    let bind f = 
        either f fail
   
    let (>>=) x f = 
        bind f x

    let (>=>) s1 s2 = 
        s1 >> bind s2
   
    let switch f = 
        f >> succeed
   
    let map f = 
        either (f >> succeed) fail
    
    let tee f x = 
        f x; x
        
    let pass f x = 
        (ignore >> f)(); x 
    
    let tryCatch f exnHandler x =
        try
            f x |> succeed
        with
        | ex -> exnHandler ex |> fail
   
    let doubleMap successFunc failureFunc =
        either (successFunc >> succeed) (failureFunc >> fail)
    
    let plus addSuccess addFailure switch1 switch2 x = 
        match (switch1 x),(switch2 x) with
        | Success s1,Success s2 -> Success (addSuccess s1 s2)
        | Failure f1,Success _  -> Failure f1
        | Success _ ,Failure f2 -> Failure f2
        | Failure f1,Failure f2 -> Failure (addFailure f1 f2)

module Seq = 

    let mapList mapper = Seq.map mapper >> Seq.toList

    let mapArray mapper = Seq.map mapper >> Seq.toArray
    
module String = 

    let notEmpty = String.IsNullOrEmpty >> not   
    
    let join sep (source: string seq) = String.Join(sep, source)