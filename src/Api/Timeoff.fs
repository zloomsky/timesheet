﻿namespace Api

module TimeOff = 

    open Entity    
    open System
    open System.Linq
    open System.Globalization
    open Utils       

    type History = {Id: int; FName: string; LName: string; Status: int; Created: DateTime;}

    type Day = {Date : DateTime; Quantity : float; Type: int}

    type Week = {Days: array<Day>; Start: DateTime; End: DateTime}

    type Timeoff = {Status: int; Weeks: array<Week>}

    let vacationTypes = [| { Value = 1; Text = "Paid" }; { Value = 0; Text = "Unpaid" } |]

    let week (day: DateTime) =
        let start = day.AddDays(-float day.DayOfWeek)
        (float >> start.AddDays)
        |> Seq.init 7
        |> Seq.map (fun day -> { Date = day.Date; Quantity = 0.0; Type = 0 })
        |> Seq.toArray
        |> fun week -> { Week.Days = week; Start = start.Date; End = start.AddDays(6.0).Date }

    let weeks (start: DateTime) (``end``: DateTime) =
        Seq.initInfinite(fun i -> start.AddDays(float(i * 7)))
        |> Seq.map week
        |> Seq.takeWhile(fun w -> w.End <= ``end``)

    let internal entries headerId = 
        let days = 
            db().TimeoffEntry
            |> source
            |> where <@ fun e -> e.TimeOffHeaderId = headerId@>
            |> sortBy <@ fun e -> e.RequestDate @>
            |> select <@ fun e -> { Date = e.RequestDate; Quantity = e.Hours; Type = e.VacationTypeId } @>
            |> query.Run 
            |> Seq.toArray           

        let header = 
            db().TimeoffHeader
            |> source
            |> find <@ fun e -> e.Id = headerId@>
            |> query.Run

        let start = 
            days
            |> Seq.head
            |> fun s -> s.Date.AddDays(-float s.Date.DayOfWeek)

        let ``end`` = 
            days
            |> Seq.last
            |> fun e -> e.Date.AddDays(6.0 - float e.Date.DayOfWeek)
        
        let merge days date = 
            match Seq.tryFind (fun d -> d.Date = date) days with
            | Some(day) -> day
            | None      -> {Date = date; Quantity = 0.0; Type = 0 }
       
        (float >> start.AddDays)
        |> Seq.init ((``end`` - start).Days + 1)
        |> Seq.map (merge days)
        |> Seq.groupBy (fun d -> d.Date.AddDays(-float d.Date.DayOfWeek))
        |> Seq.map (fun (start, days) ->  { Week.Days = Array.ofSeq days; Start = start; End = start.AddDays(7.0) })
        |> Seq.filter (fun w -> (Seq.sumBy (fun d -> d.Quantity) w.Days) > 0.0)
        |> Array.ofSeq
        |> fun w -> {Status = header.Status; Weeks = w}
        
    let timeoffEntries (headerId: Nullable<int>) = 
        match Option.ofNullable headerId with
        | Some(id) -> entries id
        | None     -> {Status = int RequestStatus.InProgress; Weeks = [||]}

    let timeoffHeaders page limit userId = 
        let db = db()
        db.TimeoffHeader
        |> source
        |> join (source (db.User)) <@ fun th -> th.EmployeeNo @> <@ fun u -> u.EmployeeNumber @> <@ fun th u -> th, u @>
        |> where <@ fun (_, u) -> u.Id = userId @>
        |> join (source (db.WFE___Live_Employee)) <@ fun (th, _) -> th.EmployeeNo @> <@ fun e -> e.No_ @> <@ fun (th, _) e -> th, e @>
        |> select <@ fun (t, e) -> { Id = t.Id; FName = e.First_Name; LName = e.Last_Name; Status = t.Status; Created = t.DateCreated} @>
        |> sortBy <@ fun t -> t.Created @>
        |> toPage page limit

    let internal mapEntries (weeks: array<Week>) userName = 
        let entry day =
            let entry = Model.ServiceTypes.TimeoffEntry()
            entry.RequestDate <- day.Date
            entry.Hours <- day.Quantity
            entry.VacationTypeId <- day.Type
            entry.ModifiedBy <- userName
            entry.LastModified <- cstNow()
            entry

        weeks
        |> Seq.collect(fun w -> w.Days)
        |> Seq.filter(fun d -> d.Quantity > 0.0)
        |> Seq.map entry
        
    let newTimeoff (data : Timeoff) userId = 
        use db = db()
        let user, employee = (employee db >> exactlyOne >> query.Run) userId
        
        let entries = mapEntries data.Weeks user.UserName
                
        let header = 
            let header = Model.ServiceTypes.TimeoffHeader()
            header.EmployeeNo <- employee.No_
            header.DateCreated <- cstNow()
            header.Department <- employee.Department
            header.EmployeeComments <- ""
            header.SupervisorComments <- ""
            header.ModifiedBy <- user.UserName
            header.LastModified <- cstNow()
            header       
        db
        |> Railway.tee (fun db -> db.TimeoffHeader.AddObject(header))
        |> fun db -> Seq.iter header.TimeoffEntry.Add entries; db
        |> fun db -> db.DataContext.SaveChanges()
        
    let updateTimeoff (data : Timeoff) headerId userId =
        use db = db()
        let user, _ = (employee db >> exactlyOne >> query.Run) userId
        
        let entries = mapEntries data.Weeks user.UserName        
        
        db.TimeoffHeader
        |> source
        |> find <@ fun h -> h.Id = headerId @>
        |> query.Run
        |> Railway.tee (fun h -> Seq.iter db.DataContext.DeleteObject (h.TimeoffEntry.ToList()))
        |> Railway.tee (fun h -> Seq.iter h.TimeoffEntry.Add entries)
        |> fun h -> h.LastModified <- cstNow(); h.ModifiedBy <- user.UserName

        db.DataContext.SaveChanges()

    let internal entryToString (entry: Model.ServiceTypes.TimeoffEntry) = 
        let vtype = Seq.find(fun v -> v.Value = entry.VacationTypeId) vacationTypes      
        String.Format(CultureInfo.InvariantCulture, "{0:MM/dd/yyyy} - {1:00} - {2}", entry.RequestDate, entry.Hours, vtype.Text)

    let updateTimeoffStatus headerId status userId = 
        use db = db()
        let user, employee = (employee db >> exactlyOne >> query.Run) userId        
       
        let header, entries = 
            db.TimeoffHeader
            |> source
            |> select <@ fun h -> h, h.TimeoffEntry @>
            |> find <@ fun(h, e) -> h.Id = headerId @>
            |> query.Run
            |> Railway.tee (fun(h, e) -> h.Status <- int status)
            |> fun (h, e) -> h.LastModified <- cstNow(); h.ModifiedBy <- user.UserName; h, e.ToList()

        db.DataContext.SaveChanges()
        |> ignore

        let entries = Seq.mapList entryToString entries              

        let statusUpdate = 
            {
                TimeoffStatusUpdate.Recipients = [employee.Company_E_Mail]
                Entries = entries
                Status = sprintf "%O" status
            } :> IEmailNotification            

        let review() = 
            {
                TimeoffInvite.Recipients = emails [employee.Manager_No_; employee.Supervisor]
                Employee = sprintf "%s %s" employee.First_Name employee.Last_Name
                Entries = entries
                Link = sprintf "review/%s/%i" employee.No_ headerId                
            } :> IEmailNotification

        if status = RequestStatus.Submitted 
        then              
            [review(); statusUpdate]
        else 
            [statusUpdate]    

     