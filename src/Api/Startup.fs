﻿namespace Api

open System.Web.Http
open System.Web.Http.ExceptionHandling
open System.Net.Http
open System.Reflection
open System.Web.Http.Filters
open System.Web.Http.Controllers
open System.Runtime.InteropServices

open global.Owin

open Newtonsoft.Json.Serialization

open Ninject
open Ninject.Modules
open Ninject.Web.Common.OwinHost
open Ninject.Web.WebApi.OwinHost

open NLog.Targets

type DefaultParameterValueFilter() =
      inherit ActionFilterAttribute() 

      let defaultValues (parameters: ParameterInfo array) =
        [ for param in parameters do
            let attrs = List.ofSeq (param.GetCustomAttributes<DefaultParameterValueAttribute>())
            match attrs with
            | [x] -> yield param, x.Value
            | [] -> () 
            | _ -> failwithf "Multiple DefaultParameterValueAttribute on param '%s'!" param.Name
        ]
  
      let setDefaultValues (context: HttpActionContext) =
        match context.ActionDescriptor with
        | :? ReflectedHttpActionDescriptor as ad ->
          let defaultValues = defaultValues (ad.MethodInfo.GetParameters())
          for (param, value) in defaultValues do
            match context.ActionArguments.TryGetValue(param.Name) with
            | true, :? System.Reflection.Missing
            | false, _ ->
              let _ = context.ActionArguments.Remove(param.Name)
              context.ActionArguments.Add(param.Name, value)
            | _, _ -> ()
        | _ -> ()
  
      override __.OnActionExecuting(context) =
        setDefaultValues context
        base.OnActionExecuting(context)
  
      override __.OnActionExecutingAsync(context, cancellationToken) =
        setDefaultValues context
        base.OnActionExecutingAsync(context, cancellationToken)

type ApiModule() =
    inherit NinjectModule()

    override this.Load() =
        this.Bind<ISessionStorage>().To<FileSessionStorage>().InSingletonScope() |> ignore

type Startup() =

    let anonymous = [
        "/api/login", [HttpMethod.Post];
        "/api/app/version", [HttpMethod.Get]; 
    ]

    member __.Configuration(app:Owin.IAppBuilder) =
        register [new LogentriesTarget(Name = "LE", Token = Utils.connection "LEToken", Layout = layout)]
        let kernel = new StandardKernel(new ApiModule()) :> IKernel
        let config = new HttpConfiguration()
        config.Formatters.Remove config.Formatters.XmlFormatter |> ignore 
        config.Formatters.JsonFormatter.SerializerSettings.ContractResolver <- DefaultContractResolver() 
        config.MapHttpAttributeRoutes()
        config.Filters.Add(DefaultParameterValueFilter())
        config.Services.Replace(typeof<IExceptionLogger>, new NLogExceptionLogger())
        config.IncludeErrorDetailPolicy <- IncludeErrorDetailPolicy.Never
       
        app.Use<RequestLogger>() |> ignore
        app.Use<TokenAuthorization>(kernel.Get<ISessionStorage>(), Map.ofList anonymous) |> ignore
        app.UseNinjectMiddleware(fun () -> kernel) |> ignore
        app.UseNinjectWebApi(config) |> ignore 

