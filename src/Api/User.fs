﻿namespace Api

module User = 

    open Entity
    open Utils
    
    let userInfo userId = 
        db().User
        |> source
        |> where <@ fun user -> user.Id = userId @>
        |> select <@ fun user -> (user.FirstName, user.LastName, user.Email) @>
        |> exactlyOne
        |> query.Run
    
    let authInfo login pin = 
        db().User
        |> source
        |> where <@ fun user -> user.UserName = login && user.Pin = pin @>
        |> select <@ fun user -> (user.FirstName, user.LastName, user.Email, user.Id) @>
        |> exactlyOneOrDefault
        |> query.Run
        |> toOption 