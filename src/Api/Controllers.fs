﻿namespace Api

open System
open System.Web.Http
open System.Reflection
open System.Drawing

open Utils
open User
open TimeSheet
open TimeOff
open Employee
open Settings
open System.IO
open System.Net.Http
open System.Net
open System.Net.Http.Headers
open System.Web.Hosting


[<RoutePrefix("api/login")>]
type LoginController(sessionStorage : ISessionStorage) = 
    inherit ApiController()
    
    let authorized controller persistent userInfo userId = 
        let expires = DateTime.UtcNow.AddDays 1.0
        let sid = sessionStorage.AddSession expires userId
        AuthorizeResult(userInfo, sid, (if persistent then Nullable(DateTimeOffset expires) else Nullable()), controller) :> IHttpActionResult
    
    let userInfo fname lname email = { UserInfo.Email = email; FirstName = fname; LastName = lname }
    
    [<Route("")>]
    member this.Get() = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> User.userInfo
        |||> userInfo
    
    [<Route("")>]
    member this.Post(credentials : Credentials) = 
        let authorized = authorized this credentials.Persistent
        match authInfo credentials.Login credentials.Password with
        | Some(fname, lname, email, id) -> authorized (userInfo fname lname email) id
        | None -> this.Unauthorized() :> IHttpActionResult
    
    [<Route("")>]
    member this.Delete() = 
        this.Request
        |> sessionKey
        |> sessionStorage.DeleteSession

[<RoutePrefix("api/timesheet")>]
type TimesheetController(sessionStorage: ISessionStorage) = 
    inherit ApiController()

    [<HttpGet>]
    [<Route("history")>]
    member this.Get([<Opt; Def(1)>]page: int, [<Opt; Def(12)>]limit: int) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> timesheetHeaders page limit

    [<HttpGet>]
    [<Route("history/{code?}/{headerId:int?}")>]
    member __.Get(code, [<Opt; Def(null)>]headerId: Nullable<int>) = 
        if headerId.HasValue then entriesById headerId.Value code else entriesByCode code

    [<HttpGet>]
    [<Route("days")>]
    member this.Get(start, ``end``) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> day start ``end``

    [<HttpPost>]
    [<Route("active")>]
    member this.Post() = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> payrollForActivePeriod        

    [<HttpPost>]
    [<Route("history/{code?}/{headerId:int?}")>]
    member this.Post(code, headerId: int, payroll: PayrollPeriod) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updatePayroll code headerId payroll
        |> ignore

    [<HttpPost>]
    [<Route("submit/{headerId:int?}")>]
    member this.Submit(headerId: int) = 
        this.Request 
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updatePayrollStatus headerId RequestStatus.Submitted
        |> Seq.iter (Background.action Email.send)

    [<HttpPost>]
    [<Route("approve/{headerId:int?}")>]
    member this.Approve(headerId: int) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updatePayrollStatus headerId RequestStatus.Approved
        |> Seq.iter (Background.action Email.send)

    [<HttpPost>]
    [<Route("reject/{headerId:int?}")>]
    member this.Reject(headerId: int) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updatePayrollStatus headerId RequestStatus.Rejected
        |> Seq.iter (Background.action Email.send)
        

[<RoutePrefix("api/timeoff")>]
type TimeoffController(sessionStorage: ISessionStorage) = 
    inherit ApiController()

    [<HttpGet>]
    [<Route("history")>]
    member this.Get([<Opt; Def(1)>]page: int, [<Opt; Def(10)>]limit: int) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> timeoffHeaders page limit 

    [<HttpGet>]
    [<Route("history/details/{headerId:int?}")>]
    member __.Get([<Opt; Def(null)>]headerId: Nullable<int>) = 
        timeoffEntries headerId 

    [<HttpGet>]
    [<Route("weeks")>]
    member __.Week(start: DateTime, ``end``: DateTime) = 
        weeks start ``end``

    [<HttpPut>]
    [<Route("history")>]
    member this.Put(timeoff: Timeoff) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> newTimeoff timeoff
        |> ignore

    [<HttpPost>]
    [<Route("history/{headerId:int}")>]
    member this.Post(headerId: int, timeoff: Timeoff) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updateTimeoff timeoff headerId
        |> ignore

    [<HttpPost>]
    [<Route("submit/{headerId:int?}")>]
    member this.Submit(headerId: int) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updateTimeoffStatus headerId RequestStatus.Submitted
        |> Seq.iter (Background.action Email.send)        

    [<HttpPost>]
    [<Route("approve/{headerId:int?}")>]
    member this.Approve(headerId: int) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updateTimeoffStatus headerId RequestStatus.Approved
        |> Seq.iter (Background.action Email.send)   

    [<HttpPost>]
    [<Route("reject/{headerId:int?}")>]
    member this.Reject(headerId: int) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> updateTimeoffStatus headerId RequestStatus.Rejected
        |> Seq.iter (Background.action Email.send)   

[<RoutePrefix("api/employee")>]
type EmployeeController(sessionStorage: ISessionStorage) = 
    inherit ApiController()

    [<HttpGet>]
    [<Route("requests/{employeeNo}")>]
    member __.GetRequests(employeeNo, [<Opt; Def(0)>]t: int, [<Opt; Def(0)>]h: int, [<Opt; Def(1)>]page: int, [<Opt; Def(12)>]limit: int) = 
        requests employeeNo t h page limit

    [<HttpGet>]
    [<Route("subordinates/{employeeNo?}")>]
    member this.Get([<Opt; Def(null)>]employeeNo) = 
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId) 
        |> layer employeeNo

    [<HttpGet>]
    [<Route("avatar/{employeeNo}")>]
    member __.GetImage(employeeNo) = 
        avatar employeeNo
        |> fun b -> Image.FromStream(new MemoryStream(b))
        |> fun i -> ImageResult(64, 64, i)


[<RoutePrefix("api/wfe")>]
type WfeController(sessionStorage: ISessionStorage) = 
    inherit ApiController()

    [<HttpGet>]
    [<Route("payrollperiods")>]
    member this.PayrollPeriods() =
        this.Request
        |> (sessionKey >> sessionStorage.GetUserId)
        |> periods
    
    [<HttpGet>]
    [<Route("payrollcodes")>]
    member __.PayrollCodes() = payrollCodes()

    [<HttpGet>]
    [<Route("jobs")>]
    member __.Jobs() = jobNumbers()

    [<HttpGet>]
    [<Route("jobs/{jobNumber?}")>]
    member __.Tasks(jobNumber) = jobTaskNumbers jobNumber

    [<HttpGet>]
    [<Route("orders")>]
    member __.Orders() = serviceOrderNumbers()

    [<HttpGet>]
    [<Route("laborcodes")>]
    member __.LaborCodes() = laborCodes() 

    [<HttpGet>]
    [<Route("workareas")>]
    member __.WorkAreas() = workAreas

    [<HttpGet>]
    [<Route("vacationtypes")>]
    member __.Vacation() = vacationTypes 


[<RoutePrefix("api/app")>]
type ApplicationController() = 
    inherit ApiController()

    [<HttpGet>]
    [<Route("version")>]
    member __.Get() = Assembly.GetExecutingAssembly().GetName().Version

    [<HttpGet>]
    [<Route("settings")>]
    member __.GetSettigs() = values()

    [<HttpGet>]
    [<Route("settings/{id}")>]
    member __.GetSettigs(id) = valueById id

    [<HttpPost>]
    [<Route("settings/{id}")>]
    member __.Post(id, valueContainer) = updateSetting id valueContainer
        

   



   

   
