﻿namespace Api

module TimeSheet = 
    
    open Entity    
    open System
    open Utils
    open System.Linq
    open System.Globalization

    type Code = {Code: string; Description: string; Payable: byte}

    type Period = { Period: string; Start: DateTime; End: DateTime; Code: string; }
    
    type History = { Id: int; FName: string; LName: string; No: string; Status: int; Period: Period }
    
    type Day = { Date: DateTime; Quantity: decimal }
    
    type Days = { Days: array<Day>; PayrollCode: string; Order: string; Job: string; LaborCode: string; WorkArea: string; JobTask: string }
    
    type PayrollPeriod = { Period: Period; Days: array<Days>; Status: int }   
    
    let entriesById headerId code =
        let period() = 
            db().TimesheetHeader
            |> source
            |> where <@ fun h -> h.Id = headerId && h.PayrollCode = code @>
            |> select <@ fun h -> { Period = h.PayrollPeriod; Start = h.PayrollStartDate; End = h.PayrollEndDate; Code = h.PayrollCode }, h.Status @>
            |> exactlyOne
            |> query.Run
        
        let map (payrollCode, jobNo, jobTaskNo, laborCode, serviceOrderNo, workArea) days = 
            let days = 
                days
                |> Seq.map (fun (day, qty) -> { Date = day; Quantity = qty})
                |> Seq.sortBy (fun day -> day.Date)
                |> Seq.toArray
            { PayrollCode = payrollCode; Job = jobNo; JobTask = jobTaskNo; LaborCode = laborCode; Order = serviceOrderNo; WorkArea = workArea; Days = days }
        
        let days() = 
            db().TimesheetEntry
            |> source
            |> where <@ fun e -> e.TimesheetHeaderId = headerId @>
            |> select <@ fun e -> (e.PayrollCode, e.JobNo, e.JobTaskNo, e.LaborCode, e.ServiceOrderNo, e.WorkArea), (e.WorkDate, e.Quantity) @>
            |> query.Run
            |> Seq.groupBy fst
            |> Seq.map (fun g -> map (fst g) (Seq.map snd (snd g)))
            |> Seq.toArray
        
        async { 
            let! days = async { return days() }
            let! period = async { return period() }
            return { Status = snd period; Period = fst period; Days = days }
        }
        |> Async.RunSynchronously
    
    let day (start : DateTime) (``end`` : DateTime) userId = 
        use db = db()
        let resourceGroup = 
            employee db userId
            |> (select <@ fun (u, e) -> e.ResourceGroup @>) 
            |> exactlyOne 
            |> query.Run

        (float >> start.AddDays)
        |> Seq.init ((``end`` - start).Days + 1)
        |> Seq.map (fun day -> { Date = day; Quantity = 0M})
        |> Seq.toArray
        |> fun days -> { Days.Days = days; PayrollCode = ""; Job = ""; JobTask = ""; LaborCode = resourceGroup; Order = ""; WorkArea = "In State" }
    
    let entriesByCode code = 
        let period = 
            db().WFE___Live_Payroll_Period
            |> source
            |> where <@ fun h -> h.Code = code @>
            |> select <@ fun h -> { Period = h.Period; Start = h.Start_Date; End = h.End_Date; Code = h.Code } @>
            |> exactlyOne
            |> query.Run
        
        { Period = period; Days = [||]; Status = 0 }
    
    let timesheetHeaders page limit userId = 
        let db = db()
        db.TimesheetHeader
        |> source
        |> join (source (db.User)) <@ fun th -> th.EmployeeNo @> <@ fun u -> u.EmployeeNumber @> <@ fun th u -> th, u @>
        |> where <@ fun (th, u) -> u.Id = userId @>
        |> join (source (db.WFE___Live_Employee)) <@ fun (th, _) -> th.EmployeeNo @> <@ fun e -> e.No_ @> <@ fun (th, _) e -> th, e @>
        |> sortBy <@ fun (th, _) -> th.DateCreated @>
        |> select <@ fun (th, e) -> { Id = th.Id; FName = e.First_Name; LName = e.Last_Name; No = th.EmployeeNo; Status = th.Status; Period = { Period = th.PayrollPeriod; Start = th.PayrollStartDate; End = th.PayrollEndDate; Code = th.PayrollCode } } @>
        |> toPage page limit
    
    let workAreas = [| { Value = "In State"; Text = "In State" }; { Value = "Out of State"; Text = "Out of State" } |]

    let periods userId = 
        use db = db()
        query {
            let employee = 
                query { 
                    for u in db.User do 
                    join e in db.WFE___Live_Employee on (u.EmployeeNumber = e.No_) 
                    where (u.Id = userId)
                    select e
                    headOrDefault                    
                }

            let usedCodes = 
                query {   
                    for t in db.TimesheetHeader do    
                    where (t.EmployeeNo = employee.No_)
                    join p in db.WFE___Live_Payroll_Period on (t.PayrollCode = p.Code)
                    select p.Code
                    distinct  
                }         
                       
            for p in db.WFE___Live_Payroll_Period do
            where (p.Payroll_Type = int employee.Salaried)
            sortByDescending p.ActivePeriod
            thenBy p.Start_Date
            if not(usedCodes.Contains(p.Code)) then 
                select { Period = p.Period; Start = p.Start_Date; End = p.End_Date; Code = p.Code }

        } |> Seq.toList  
    
    let payrollCodes() = 
        db().WFE___Live_Payroll_Code
        |> source
        |> where <@ fun c -> c.Show_on_Time_Sheet = 1uy @>
        |> select <@ fun c -> {Code.Code = c.Code; Description = c.Description; Payable = c.Payable } @>
        |> query.Run
    
    let jobNumbers() = 
        db().WFE___Live_Job
        |> source
        |> where <@ fun j -> j.Job_Status = 1 || j.Job_Status = 2 @>
        |> select <@ fun j -> { Value = j.No_; Text = j.Description } @>
        |> query.Run
    
    let jobTaskNumbers jobNumber = 
        db().WFE___Live_Job_Task
        |> source
        |> where <@ fun jt -> jt.Job_No_ = jobNumber && jt.No_ = "LABOR" @>
        |> select <@ fun jt -> { Value = jt.Job_Task_No_; Text = jt.Description } @>
        |> query.Run
    
    let serviceOrderNumbers() = 
        db().WFE___Live_Service_Header
        |> source
        |> where <@ fun so -> so.Document_Type = 1 @>
        |> select <@ fun so -> { Value = so.No_; Text = so.Bill_to_Name } @>
        |> query.Run
    
    let laborCodes() = 
        db().WFE___Live_Resource
        |> source
        |> where <@ fun lc -> lc.LaborCode = 1uy @>
        |> select <@ fun lc -> { Value = lc.No_; Text = lc.Name } @>
        |> query.Run
    
    let employeeType userId = 
        use db = db()
        employee db userId
        |> select <@ fun (_, e) -> e.Salaried @>
        |> exactlyOne
        |> query.Run
    
    let internal ordersInfoes (orders : seq<string>) = 
        use db = db()
        db.WFE___Live_Service_Header
        |> source
        |> where <@ fun h -> orders.Contains(h.Service_Order_No_) @>
        |> select <@ fun h -> h.No_, h.Shortcut_Dimension_1_Code, h.Shortcut_Dimension_2_Code, h.Bill_to_Name @>
        |> query.Run
        |> Seq.toArray
    
    let internal jobsInfoes (jobs : seq<string>) = 
        db().WFE___Live_Job
        |> source
        |> where <@ fun j -> jobs.Contains(j.No_) @>
        |> select <@ fun j -> j.No_, j.Global_Dimension_1_Code, j.Global_Dimension_2_Code, j.Bill_to_Name @>
        |> query.Run
        |> Seq.toArray
    
    let internal entries days userName unitCost department manager = 
        async { 
            let! ordersInfoes = async { return (Seq.map (fun d -> d.Order) >> Seq.distinct >> ordersInfoes) days }
            let! jobsInfoes = async { return (Seq.map (fun d -> d.Job) >> Seq.distinct >> jobsInfoes) days }
            
            let entry days day = 
                let entry = Model.ServiceTypes.TimesheetEntry()
                let _, profitCenter, projectManager, customerName = 
                    match String.notEmpty days.Order, String.notEmpty days.Job with
                    | false, true -> Seq.find (fun (no, _, _, _) -> no = days.Job) jobsInfoes
                    | true, false -> Seq.find (fun (no, _, _, _) -> no = days.Order) ordersInfoes
                    | false, false -> null, department, manager, null
                    | true, true -> failwith "Job and order selected."

                entry.PayrollCode <- days.PayrollCode
                entry.Quantity <- day.Quantity
                entry.WorkDate <- day.Date
                entry.WorkArea <- days.WorkArea
                entry.JobNo <- days.Job
                entry.ServiceOrderNo <- days.Order
                entry.UnitCost <- Nullable unitCost
                entry.ProfitCenter <- profitCenter
                entry.ProjectManager <- projectManager
                entry.JobTaskNo <- days.JobTask
                entry.CustomerName <- customerName
                entry.LaborCode <- days.LaborCode
                entry.LastModified <- cstNow()
                entry.ModifiedBy <- userName
                entry
            
            let entries = 
                days 
                |> Seq.collect (fun days -> Seq.map (entry days) days.Days) 

            return entries
        }
    
    let payrollForActivePeriod userId = 
        use db = db()
        let user, employee = (employee db >> exactlyOne >> query.Run) userId
        
        let activePeriod = 
            db.WFE___Live_Payroll_Period
            |> source
            |> where <@ fun p -> p.ActivePeriod = 1uy && p.Payroll_Type = int employee.Salaried @>
            |> exactlyOne
            |> query.Run

        let activeHeader =
            db.TimesheetHeader
            |> source
            |> where <@ fun t -> t.PayrollPeriod = activePeriod.Period && t.EmployeeNo = employee.No_ @>
            |> headOrDefault
            |> query.Run

        if isNull activeHeader
        then           
            let header = 
                let header = Model.ServiceTypes.TimesheetHeader()
                header.EmployeeNo <- employee.No_
                header.DateCreated <- cstNow()
                header.ManagerNo <- employee.Manager_No_
                header.PayrollPeriod <- activePeriod.Period
                header.PayrollStartDate <- activePeriod.Start_Date
                header.PayrollEndDate <- activePeriod.End_Date
                header.LastModified <- cstNow()
                header.PayrollCode <- activePeriod.Code
                header.EmployeeComments <- ""
                header.SupervisorComments <- ""
                header.AdministratorComments <- ""
                header.ModifiedBy <- user.UserName
                header       
       
            db
            |> Railway.tee (fun db -> db.TimesheetHeader.AddObject(header))        
            |> fun db -> db.DataContext.SaveChanges()
            |> ignore

            header.Id, activePeriod.Code
        else 
            activeHeader.Id, activeHeader.PayrollCode
    
    let updatePayroll code headerId data userId = 
        use db = db()
        let user, employee = (employee db >> exactlyOne >> query.Run) userId
        
        let entries = 
            entries data.Days user.UserName employee.UnitCost employee.Department employee.Manager_No_ 
            |> Async.RunSynchronously
        
        db.TimesheetHeader
        |> source
        |> find <@ fun h -> h.Id = headerId && h.PayrollCode = code @>
        |> query.Run
        |> Railway.tee (fun h -> Seq.iter db.DataContext.DeleteObject (h.TimesheetEntry.ToList()))
        |> Railway.tee (fun h -> Seq.iter h.TimesheetEntry.Add entries)
        |> fun h -> h.LastModified <- cstNow(); h.ModifiedBy <- user.UserName

        db.DataContext.SaveChanges()

    let internal entryToString (entry: Model.ServiceTypes.TimesheetEntry) = 
        let details = 
            [entry.PayrollCode; entry.LaborCode; entry.WorkArea; entry.JobNo; entry.JobTaskNo; entry.ServiceOrderNo]
            |> Seq.filter String.notEmpty
            |> String.join " - "

        String.Format(CultureInfo.InvariantCulture, "{0:MM/dd/yyyy} - {1:00} - {2}", entry.WorkDate, entry.Quantity, details)
   
    let updatePayrollStatus headerId status userId  = 
        use db = db()
        let user, employee = (employee db >> exactlyOne >> query.Run) userId        
       
        let header, entries = 
            db.TimesheetHeader
            |> source
            |> select <@ fun h -> h, h.TimesheetEntry @>
            |> find <@ fun (h, _) -> h.Id = headerId @>
            |> query.Run
            |> Railway.tee (fun (h, _) -> h.Status <- int status)
            |> fun (h, e) -> h.LastModified <- cstNow(); h.ModifiedBy <- user.UserName; h, e

        db.DataContext.SaveChanges() 
        |> ignore

        let entries = Seq.mapList entryToString entries 

        let statusUpdate = 
            {
                TimesheetStatusUpdate.Recipients = [employee.Company_E_Mail]
                Period = header.PayrollPeriod
                Entries = entries
                Status = sprintf "%O" status
            } :> IEmailNotification       
         
        let review() = 
            {
                TimesheetInvite.Employee = sprintf "%s %s" employee.First_Name employee.Last_Name
                Period = header.PayrollPeriod
                Link = sprintf "review/%s/%i/%s" employee.No_ headerId header.PayrollCode
                Recipients = emails [employee.Manager_No_; employee.Supervisor]
            } :> IEmailNotification        

        if status = RequestStatus.Submitted 
        then  
            [review(); statusUpdate]
        else
            [statusUpdate]

