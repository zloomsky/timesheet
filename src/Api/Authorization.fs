﻿namespace Api

open Microsoft.Owin

open Newtonsoft.Json

open System
open System.Collections.Concurrent
open System.Collections.Generic
open System.IO
open System.Net
open System.Net.Http
open System.Net.Http.Headers
open System.Security.Cryptography
open System.Text
open System.Text.RegularExpressions
open System.Threading.Tasks
open System.Timers
open System.Web.Http
open System.Web.Http.Results

[<AutoOpen>]
module Authorization = 
    let check (password : string) (salt : string) (hash : string) = 
        [ Convert.FromBase64String salt; Encoding.Unicode.GetBytes password ]
        |> Array.concat
        |> HashAlgorithm.Create("SHA1").ComputeHash
        |> Convert.ToBase64String
        |> hash.Equals
    
    let sessionKey (request : HttpRequestMessage) = 
        request.Headers.GetCookies("sid")
        |> Seq.map (fun c -> c.["sid"].Value)
        |> Seq.head
        |> Guid.Parse

type Session = { Expires : DateTime; UserId : int }

type Credentials = { Login : string; Password : string; Persistent : bool }

type UserInfo = { FirstName : string; LastName : string; Email : string }

type AuthorizeResult(userInfo, sessionId, expires, controller : ApiController) = 
    inherit OkNegotiatedContentResult<UserInfo>(userInfo, controller)
    let cookie = [ new CookieHeaderValue("sid", sessionId.ToString(), Expires = expires, HttpOnly = true) ]
    member __.Execute token = base.ExecuteAsync token
    override this.ExecuteAsync token = 
        async { 
            let! response = this.Execute(token) |> Async.AwaitTask
            response.Headers.AddCookies(cookie)
            return response
        }
        |> Async.StartAsTask

type ISessionStorage = 
    abstract AddSession : DateTime -> int -> Guid
    abstract IsValidKey : Guid -> bool
    abstract GetUserId : Guid -> int
    abstract DeleteSession : Guid -> unit

type FileSessionStorage() = 
    let path = Path.Combine(Path.GetTempPath(), "timesheet")
    
    let file = Path.Combine(path, "sessions.json")

    do if (Directory.Exists >> not) path then (Directory.CreateDirectory >> ignore) path
    
    let sessions = 
        if File.Exists(file) 
        then File.ReadAllText(file)
        else File.WriteAllText(file, "{}"); "{}"
        |> fun text -> JsonConvert.DeserializeObject<Dictionary<Guid, Session>>(text)
        |> Seq.where (fun s -> s.Value.Expires > DateTime.UtcNow)
        |> ConcurrentDictionary
    
    let ticker = new Timer(TimeSpan.FromMinutes(1.0).TotalMilliseconds)
    
    let remove id = 
        match sessions.TryRemove id with
        | true, _ -> File.WriteAllText(file, JsonConvert.SerializeObject(sessions))
        | false, _ -> ignore()
    
    let tick _ = 
        sessions
        |> Seq.where (fun s -> s.Value.Expires <= DateTime.UtcNow)
        |> Seq.map (fun s -> s.Key)
        |> Seq.toArray
        |> Seq.iter remove
    
    do 
        ticker.Elapsed.Add tick
        ticker.Start()
    
    interface ISessionStorage with
        member __.DeleteSession sessionId = remove sessionId
        
        member __.GetUserId sessionId = 
            match sessions.TryGetValue sessionId with
            | true, session -> session.UserId
            | false, _ -> failwith "Session not found."
        
        member __.AddSession expires userId = 
            let sessionId = Guid.NewGuid()
            match sessions.TryAdd(sessionId, { Expires = expires; UserId = userId }) with
            | true -> File.WriteAllText(file, JsonConvert.SerializeObject(sessions)); sessionId
            | false -> failwith "Can't create new session."
        
        member __.IsValidKey sessionId = sessions.ContainsKey sessionId

type TokenAuthorization(next : OwinMiddleware, sessionStorage : ISessionStorage, anonymous : Map<string, list<HttpMethod>>) = 
    inherit OwinMiddleware(next)

    let compare (request : IOwinRequest) pattern (methods : list<HttpMethod>) = 
        Regex.IsMatch(request.Path.ToString(), pattern, RegexOptions.IgnoreCase)  && methods 
        |> Seq.exists (fun m -> request.Method = m.Method)

    let next context = next.Invoke(context) |> Async.AwaitTask

    override __.Invoke(context) = 
        async { 
            let compare = compare context.Request
            match (anonymous |> Map.exists compare, Guid.TryParse context.Request.Cookies.["sid"]) with
            | true, (_, _)                               -> do! next context
            | false, (true, sessionId)
                when sessionStorage.IsValidKey sessionId -> do! next context
            | _                                          -> context.Response.StatusCode <- int HttpStatusCode.Unauthorized
        }
        |> Async.StartAsTask :> Task