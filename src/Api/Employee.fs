﻿namespace Api


module Employee = 
    
    open Entity
    open Utils
    open TimeSheet
    open FSharp.Data
    open System.Data.Objects
    open System.Data.SqlClient
    open System.Data
    open System.Linq
    open System

    type Employee = {No: string; FName: string; MName: string; LName: string; Manager: string; TSheets: int; TOffs: int; Sub: int; Norm: int}

    type Layer = {Chief: Employee; Subordinates: seq<Employee>}

    type Request = {Id: int; Type: int; FName: string; LName: string; Created: DateTime; Status: int; Period: Period}

    let subordinates managerNo = 
        let db = db()
        query {
            for e in db.WFE___Live_Employee do
            let manager = if (String.IsNullOrEmpty(e.Manager_No_)) then e.Supervisor else e.Manager_No_
            let subordinates = 
                query { 
                    for s in db.WFE___Live_Employee do 
                    where ((if (String.IsNullOrEmpty(s.Manager_No_)) then s.Supervisor else s.Manager_No_) = e.No_) 
                    count 
                }
            let timesheets = 
                query {
                    for t in db.TimesheetHeader do 
                    where (t.EmployeeNo = e.No_ && t.Status = int RequestStatus.Submitted) 
                    count
                }
            let timeoffs = 
                query {
                    for t in db.TimeoffHeader do 
                    where (t.EmployeeNo = e.No_ && t.Status = int RequestStatus.Submitted) 
                    count
                }
            where (manager = managerNo)
            select {No = e.No_; FName = e.First_Name; MName = e.Middle_Name; LName = e.Last_Name; Manager = manager; TSheets = timesheets; TOffs = timeoffs; Sub = subordinates; Norm = 0}
        }
        |> Seq.toList
    
    let chief employeeNo = 
        use db = db()
        query {
            for e in db.WFE___Live_Employee do
            let manager = if (String.IsNullOrEmpty(e.Manager_No_)) then e.Supervisor else e.Manager_No_
            let subordinates = 
                query { 
                    for s in db.WFE___Live_Employee do 
                    where ((if (String.IsNullOrEmpty(s.Manager_No_)) then s.Supervisor else s.Manager_No_) = e.No_) 
                    count 
                }
            let timesheets = 
                query {
                    for t in db.TimesheetHeader do 
                    where (t.EmployeeNo = e.No_ && t.Status = int RequestStatus.Submitted) 
                    count
                }
            let timeoffs = 
                query {
                    for t in db.TimeoffHeader do 
                    where (t.EmployeeNo = e.No_ && t.Status = int RequestStatus.Submitted) 
                    count
                }
            where (e.No_ = employeeNo)
            select {No = e.No_; FName = e.First_Name; MName = e.Middle_Name; LName = e.Last_Name; Manager = manager; TSheets = timesheets; TOffs = timeoffs; Sub = subordinates; Norm = 0}
            exactlyOne
        }

    let normalize data =
        if Seq.isEmpty data 
        then 
            data
        else
            let min, max =
                data
                |> Seq.map(fun e -> e.TOffs + e.TSheets)
                |> fun s -> Seq.min s, Seq.max s
      
            data
            |> Seq.map (fun e -> {e with Norm = if(max - min = 0) then 0 else (e.TOffs + e.TSheets - min) / (max - min) })
            |> Seq.sortByDescending (fun e -> e.TSheets + e.TOffs + e.Sub)
            
    let layer managerNo userId = 
        use db = db()
        let managerNo =
            if (String.IsNullOrEmpty managerNo)
            then (employee db >> select <@ fun (_, e) -> e.No_ @> >> exactlyOne >> query.Run) userId
            else managerNo 

        let chief() = async { return  chief managerNo}
        let subordinates() = async { return (subordinates >> normalize) managerNo}
        async { 
            let! chief = chief()
            let! subordinates = subordinates()
            return  {Chief = chief; Subordinates = subordinates}
        }
        |> Async.RunSynchronously

    let avatar employeeNo = 
        db().WFE___Live_Employee
        |> source
        |> where <@ fun e -> e.No_ = employeeNo @>
        |> select <@ fun e -> e.Picture @>
        |> headOrDefault
        |> query.Run
        
    let requestsQuery = "select 
                            data.*,
	                        e.[First Name],
	                        e.[Last Name]
                        from(select 
		                        Id,
		                        Type = 1,
		                        EmployeeNo, 
		                        DateCreated,
		                        Status,
		                        PayrollPeriod,
		                        PayrollStartDate,
		                        PayrollEndDate,
		                        PayrollCode
	                        from TimesheetHeader
	                        union all
	                        select
		                        Id,
		                        Type = 2,
		                        EmployeeNo,
		                        DateCreated,
		                        Status,
		                        null as PayrollPeriod,
		                        null as PayrollStartDate,
		                        null as PayrollEndDate,
		                        null as PayrollCode
	                        from TimeoffHeader) as data
                        left join [WFE - Live$Employee] as e on data.EmployeeNo = e.No_
                        where data.EmployeeNo = @empNo and (@type = 0 or data.Type = @type) and (@history = 1 or data.Status = 1) and (@history = 0 or (data.Status >= 1))
                        order by DateCreated
                        offset @skip rows fetch next @take rows only"

    let requestsCountQuery = "select count(*) 
                              from(select Id, EmployeeNo, Type = 1, Status  from TimesheetHeader union all select Id, EmployeeNo, Type = 1, Status from TimeoffHeader) as data
                              where data.EmployeeNo = @empNo and (@type = 0 or data.Type = @type) and (@history = 1 or data.Status = 1) and (@history = 0 or (data.Status >= 1))"
    
    let requests (empNo: string) (itemType: int) (history: int) (page: int) (limit: int) =
       
        let parameters() = [|
            SqlParameter("empNo", empNo);
            SqlParameter("type", itemType);
            SqlParameter("history", history);
            SqlParameter("skip", (page - 1) * limit);
            SqlParameter("take", limit)
        |]

        let items() = 
            use connection = new SqlConnection(Entity.connection)
            use command = new SqlCommand(requestsQuery, connection)
            command.Parameters.AddRange(parameters())
            connection.Open()
            let mutable items = []
            let r = command.ExecuteReader()
            let dateTimeNull i = if r.IsDBNull(i) then DateTime.Now else r.GetDateTime(i)
            let stringNull i = if r.IsDBNull(i) then "" else r.GetString(i)
            
            while r.Read() do 
                let period =  {Period = stringNull 5; Start = dateTimeNull 6; End = dateTimeNull 7; Code = stringNull 8}
                let item = {Request.Id = r.GetInt32(0); Type = r.GetInt32(1); Created = r.GetDateTime(3); Status = r.GetInt32(4); FName = r.GetString(9); LName = r.GetString(10); Period = period}
                items <- items @ [item]
            items
            |> Seq.toArray

        let count() = 
            use connection = new SqlConnection(Entity.connection)
            use command = new SqlCommand(requestsCountQuery, connection)
            command.Parameters.AddRange(parameters())
            connection.Open()
            command.ExecuteScalar() :?> int

        let items() = async { return items() }
        let total() = async { return count() }
        async { 
            let! items = items()
            let! total = total()
            return { Page.Total = total; Items = items }
        }
        |> Async.RunSynchronously
        
    