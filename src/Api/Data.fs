﻿namespace Api

open FSharp.Data.TypeProviders

open System
open System.Text
open System.Web.Http
open System.Linq

open Utils
open System.Threading.Tasks
open System.Net.Http
open System.Net
open System.IO
open System.Drawing
open System.Net.Http.Headers

module internal Entity = 

    type Model = SqlEntityConnection<ConnectionStringName = "database", LocalSchemaFile = "database.ssdl", ForceUpdate = false>
    
    type Context = Model.ServiceTypes.SimpleDataContextTypes.EntityContainer
    
    let db() = Model.GetDataContext()

    let connection = connection "database"

    let internal employee (db:Context) userId =        
        db.User
        |> source
        |> where <@ fun u -> u.Id = userId @>
        |> join (source (db.WFE___Live_Employee)) <@ fun u -> u.EmployeeNumber @> <@ fun e -> e.No_ @> <@ fun u e -> u, e @>

    let internal emails (empNumbers: string seq) = 
        use db = db()
        db.WFE___Live_Employee
        |> source
        |> where <@ fun e -> empNumbers.Contains(e.No_) @>
        |> select <@ fun e -> e.E_Mail @>
        |> query.Run
        |> Seq.toArray 

type ValueContainer<'t> = {Value: 't}

type ImageResult(width, height, image: Image) =

    let response() = 
        let stream = new MemoryStream();
        let resized = resizeImage width height image
        resized.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg)
        let response = new HttpResponseMessage(HttpStatusCode.OK)
        response.Content <- new ByteArrayContent(stream.ToArray())
        response.Content.Headers.ContentType <- new MediaTypeHeaderValue("image/jpeg")
        response

    interface IHttpActionResult with
        member __.ExecuteAsync token = Task.Run(response, token)

type RequestStatus = InProgress = 0 | Submitted = 1 | Approved = 2 | Rejected = 3

type EmailConfig = {Server: string; Port: int; User: string; Password: string; Ssl: bool}

type IEmailNotification = abstract member Recipients: string seq

type TimesheetInvite = 
    {Employee: string; Period: string; Link: string; Recipients: string seq}
    interface IEmailNotification with
            member this.Recipients = this.Recipients

type TimesheetStatusUpdate = 
    {Entries: string seq; Status: string; Period: string; Recipients: string seq}
    interface IEmailNotification with
            member this.Recipients = this.Recipients

type TimeoffInvite = 
    {Employee: string; Entries: string seq; Link: string; Recipients: string seq}
    interface IEmailNotification with
            member this.Recipients = this.Recipients

type TimeoffStatusUpdate = 
    {Entries: string seq; Status: string; Recipients: string seq}
    interface IEmailNotification with
            member this.Recipients = this.Recipients
   

  
        
        
            
        

        
    