﻿namespace Api

module Email = 
    open System
    open System.Net
    open System.Net.Mail
    open Railway
    

    let private add adresses (message: MailMessage) = 
        adresses
        |> Seq.map MailAddress
        |> Seq.iter message.To.Add
        message

    let private host = Uri(Utils.config("host"))

    let private sendNotification body subj recipients =
        let config = Settings.getEmailConfig()
        let message = 
            new MailMessage(From = MailAddress(config.User), Subject = subj, Body = body, IsBodyHtml = true)
            |> add recipients        

        use client = new SmtpClient(host = config.Server, port = config.Port, Credentials = NetworkCredential(config.User, config.Password), EnableSsl = config.Ssl)
        client.Send(message)    

    let sendTimesheetReview (msg: TimesheetInvite) = 
        let templ =  sprintf "<p>%s</p><p>Payroll Period - %s</p><p>Please browse to <a href='%s'>link</a> to approve/reject.</p>"
        let subject = sprintf "Please review Timesheet Request by %s" msg.Employee
        let link = Uri(host, msg.Link)
        let body = templ msg.Employee msg.Period link.AbsoluteUri   
        
        sendNotification body subject msg.Recipients   
        
    let sendTimesheetStatusUpdate (msg: TimesheetStatusUpdate) =        
        let subject = sprintf "Timesheet request Period %s is %s" msg.Period msg.Status       
        let body = Seq.reduce (fun s h -> s + sprintf "<br>%s</br>" h) msg.Entries
        
        sendNotification body subject msg.Recipients  

    let sendTimeoffReview (msg: TimeoffInvite) = 
        let templ = sprintf "<p>%s</p>%s<p>Please browse to <a href='%s'>link</a> to approve/reject.</p>" 
        let subject = sprintf "Please review Timeoff Request by %s" msg.Employee
        let link = Uri(host, msg.Link)
        let hours = Seq.reduce (fun s h -> s + sprintf "<br>%s</br>" h) msg.Entries
        let body = templ msg.Employee hours link.AbsoluteUri    
        
        sendNotification body subject msg.Recipients
    
    let sendTimeoffStatusUpdate (msg: TimeoffStatusUpdate) =        
        let subject = sprintf "Timeoff request is %s" msg.Status       
        let body = Seq.reduce (fun s h -> s + sprintf "<br>%s</br>" h) msg.Entries         
        
        sendNotification body subject msg.Recipients

    let send (msg: IEmailNotification) = 
        match msg with
        | :? TimeoffStatusUpdate as m   -> sendTimeoffStatusUpdate m
        | :? TimeoffInvite as m         -> sendTimeoffReview m
        | :? TimesheetInvite as m       -> sendTimesheetReview m
        | :? TimesheetStatusUpdate as m -> sendTimesheetStatusUpdate m
        | _                             -> failwith "Unknown message type."
        
        
        
        

        