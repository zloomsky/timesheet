﻿namespace Api

module Settings =
    
    open Entity
    open Utils
    open System

    type Setting = {Description: string; Value: string; Id: string}

    let hideValue = Seq.take 3 >> Seq.toArray >> String >> sprintf "%s..."

    let values() = 
        db().Settings
        |> source  
        |> select <@ fun s -> s.Description, s.Value, s.Secure, s.Id @>              
        |> query.Run
        |> Seq.map (fun (d, v, s, id) -> {Description = d; Value = (if s then hideValue v else v); Id = id})

    let valueById id =        
        db().Settings
        |> source    
        |> find <@ fun s -> s.Id = id @>
        |> query.Run
        |> fun s -> if s.Secure then hideValue s.Value else s.Value       

    let updateSetting id (valueContainer: ValueContainer<string>) = 
        let db = db()       
        db.Settings
        |> source    
        |> find <@ fun s -> s.Id = id @>
        |> query.Run
        |> fun s -> s.Value <- valueContainer.Value

        db.DataContext.SaveChanges()

    let private foldEmailConfig config (key, value) =
        match key with 
        | "EnableSsl"       -> {config with Ssl = bool.Parse value}
        | "SMTPPassword"    -> {config with Password = value}
        | "SMTPPort"        -> {config with Port = Int32.Parse value}
        | "SMTPServer"      -> {config with Server = value}
        | "SMTPUsername"    -> {config with User = value}
        | _                 -> config

    let getEmailConfig() =     
        let defaultConfig = {EmailConfig.Password = ""; Server = ""; Port = 0; User = ""; Ssl = false}
        db().Settings
        |> source    
        |> select <@ fun s -> s.Id, s.Value @>                 
        |> query.Run
        |> Seq.map id
        |> Seq.fold foldEmailConfig defaultConfig


        
        


      