namespace Api

open Microsoft.Owin

open System
open System.Threading.Tasks
open System.Reflection

open NLog
open NLog.Fluent
open NLog.Config
open NLog.Layouts
open System.Web.Http.ExceptionHandling

[<AutoOpen>]
 module Logging =

    let layout = 
        let version = Assembly.GetExecutingAssembly().GetName().Version
        let layout = sprintf "${date:format= HH\:mm\:ss.fff zzz} %O ${uppercase:${level}} ${message} ${exception:maxInnerExceptionLevel=15}" version
        Layout.FromString(layout)

    let register targets  = 
        let config = LoggingConfiguration()
        let addTarget target =
            config.LoggingRules.Add(LoggingRule("*", LogLevel.Info, target))
            config.AddTarget(target)

        targets 
        |> Seq.iter addTarget
                       
        LogManager.Configuration <- config

type NLogExceptionLogger() = 
    interface IExceptionLogger with
        member __.LogAsync(context, _) =
            async {Log.Error().Exception(context.Exception).Write()}
            |> Async.StartAsTask :> Task

type RequestLogger(next : OwinMiddleware) = 
    inherit OwinMiddleware(next)
    
    let msg (context : IOwinContext) = 
        let ip = sprintf "%s:%O" context.Request.RemoteIpAddress context.Request.RemotePort
        let x_forwarded_for = context.Request.Headers.Get("x-forwarded-for")
        let httpMethod = context.Request.Method
        let path = context.Request.Uri.PathAndQuery
        let status = context.Response.StatusCode
        sprintf "%s - %s %i %s" (if String.IsNullOrEmpty x_forwarded_for then ip else x_forwarded_for) httpMethod status path
    
    override __.Invoke(context) = 
        async {
            let! result = next.Invoke context |> (Async.AwaitTask >> Async.Catch)
            match result with
            | Choice1Of2 _   -> Log.Info().Message(msg context).Write()
            | Choice2Of2 exn -> Log.Error().Message(msg context).Exception(exn).Write()
        }
        |> Async.StartAsTask :> Task

    