'use strict';

var path = require('path');
var extractTextPlugin = require('extract-text-webpack-plugin');
var webpack = require('webpack');
var htmlWebpackPlugin = require('html-webpack-plugin');

const rel = process.env.NODE_ENV == 'rel';

var config = {
	context: path.resolve(__dirname, 'app'),

	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'index.[chunkhash].js',
		publicPath: '/'		
	},

	resolve: {
		root: path.resolve(__dirname, 'app'),
		alias: {
			utils: 'components/common/utils',	
			components: 'components'		
		},
		extensions: ['', '.js']
	},

	module: {
		loaders: [
			{ test: /\.scss$/, loader: extractTextPlugin.extract('css!sass') },
			{ test: /\.(woff2|woff|eot|ttf|svg|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file?name=content/[name].[ext]' },
			{ 
				test: /\.js$/, 
				loader: 'babel?presets[]=es2015',   
				exclude: [path.resolve(__dirname, 'node_modules')]
			}
		]
	},

	entry: {
		index: ['./index.js']
	},

	plugins: [
        new extractTextPlugin('index.[contenthash].css', { allChunks: true }),		
        new htmlWebpackPlugin({
			filename: './index.html',
			template: 'index.html',
			chunks: ['index']
        })
    ],

};

if (rel) {
	console.log('Development environment disabled.');
	config.plugins.push(new webpack.optimize.UglifyJsPlugin({ sourceMap: false, mangle: true, comments: false }));
} else {
	var host = 'localhost';
	var port = '5000';
	console.log('Development environment enabled.');
	config.entry.index.push('webpack-dev-server/client?http://' + host + ':' + port);
	config.devtool = 'source-map';
	config.devServer = {
		host: host,
		port: port,
		contentBase: path.resolve(__dirname, 'build'),
		historyApiFallback: true,
		proxy: {
			'/api/*': {
				target: 'http://localhost:4001/',
				secure: false
			}
		}
	};
}

module.exports = config;
