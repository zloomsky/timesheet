module.exports = config;

config.$inject = ['$httpProvider'];
function config($httpProvider) {
    $httpProvider.interceptors.push(internalServerError);
}

internalServerError.$inject = ['$q', '$injector'];
function internalServerError($q, $injector) {

    return {
        'responseError': function (rejection) {
            if (rejection.status >= 500 && rejection.status < 600) {
                var $mdToast = $injector.get('$mdToast');

                rejection.data.ExceptionMessage
                $mdToast.show({                   
                    locals: {message: 'Internal server error! Try again later.'},
                    position: 'bottom right',
                    controller: 'ToastController',
                    controllerAs: 'vm',
                    templateUrl: 'error-toast',
                });
            }
            return $q.reject(rejection);
        }
    };

}