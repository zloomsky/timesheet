module.exports = config;

config.$inject = ['$mdThemingProvider', '$mdIconProvider', '$mdAriaProvider', '$compileProvider'];
function config($mdThemingProvider, $mdIconProvider, $mdAriaProvider, $compileProvider) {
    var theme = $mdThemingProvider.theme('default');    
    theme.primaryPalette('blue')
        .accentPalette('light-blue')
        .warnPalette('red');

    $mdAriaProvider.disableWarnings();
    $mdIconProvider.fontSet('md', 'material-icons');
    //datepicker scroll fix
    $compileProvider.preAssignBindingsEnabled(true);
}