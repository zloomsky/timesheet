module.exports = config;

config.$inject = ['$stateProvider', '$urlRouterProvider', '$injector', '$locationProvider'];
function config($stateProvider, $urlRouterProvider, $injector, $locationProvider) {
    //load states 
    var states = [];
    var context = require.context('components', true, /routes.js$/);
    context.keys().forEach(key => states = states.concat(context(key)()));    

    var configs = flatten(states);

    configs.forEach(c => c.view.resolve = c.view.resolve || {});
    //add auth resolver
    configs.forEach(c => angular.extend(c.view.resolve, check(c)));
    //add controller resolver
    configs.filter(c => $injector.has(c.view.controller + 'Provider'))
        .forEach(c => angular.extend(c.view.resolve, $injector.get(c.view.controller + 'Provider').$get));

    states.forEach(s => $stateProvider.state(s.name, s.config))

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);
}

function check(c) { 
    if (c.name === 'login')
        return { auth: checkLoginDisabled }  

    return { auth: checkLoggedIn }
}

function flatten(states) {
    var values = (obj, name) => Object.keys(obj).map(key => ({view: obj[key], name: name}));
    var nested = states.map(s => s.config.views? values(s.config.views, s.name) : [{view: s.config, name: s.name}]);   
    return [].concat.apply([], nested);  
}

checkLoggedIn.$inject = ['$state', 'UserService'];
function checkLoggedIn($state, UserService) {
    return UserService.getCurrentUser().catch(err => $state.go('login'));   
}

checkLoginDisabled.$inject = ['$q', '$state', 'UserService'];
function checkLoginDisabled($q, $state, UserService) {
    return UserService.getCurrentUser()
    .then(u => $q.reject(false))
    .catch(user => user? $q.resolve() : $state.go('main'));    
}




