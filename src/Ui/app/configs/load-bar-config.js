module.exports = config;

config.$inject = ['cfpLoadingBarProvider'];
function config(cfpLoadingBarProvider) {
     cfpLoadingBarProvider.includeSpinner = false;
     cfpLoadingBarProvider.latencyThreshold = 0; 
}

