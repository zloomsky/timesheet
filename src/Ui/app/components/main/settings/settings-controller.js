module.exports = { name: 'SettingsController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http'];
function resolve($http) {
    return $http.get('api/app/settings').then(r => r.data);
}

controller.$inject = ['$http', '$mdDialog', 'data'];
function controller($http, $mdDialog, data) {
    var vm = this;
        
    vm.data = data;
    vm.edit = edit;

    ////   

    function edit(setting) {
        $mdDialog.show({
            locals: { description: setting.Description },
            controller: 'EditController',
            controllerAs: 'vm',
            template: require('html!./edit.html'),
            clickOutsideToClose: true,
            fullscreen: true
        })
        .then(value => {setting.updating = true; return value;})
        .then(value => $http.post(`api/app/settings/${setting.Id}`, { Value: value }))
        .then(_ => $http.get(`api/app/settings/${setting.Id}`))
        .then(r => setting.Value = r.data)
        .finally(_ => setting.updating = false);
    }

};

