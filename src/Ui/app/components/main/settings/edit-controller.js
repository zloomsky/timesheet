module.exports = { name: 'EditController', body: controller };

controller.$inject = ['$http', '$mdDialog', 'description'];
function controller($http, $mdDialog, description) {
    var vm = this;    

    vm.value = '';
    vm.description = description;
    vm.cancel = $mdDialog.cancel;
    vm.ok = ok;

    ////

    function ok() {
        $mdDialog.hide(vm.value);
    }

};
