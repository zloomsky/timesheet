module.exports = states;

function states() {
    var states = [];
    var state = (name, config) => states.push({ name: name, config: config });

    state('main.settings', {
        url: 'settings',
        views: {
            'content@main': {
                template: require('html!components/main/settings/settings.html'),
                controller: 'SettingsController',
                controllerAs: 'vm'
            }
        },
        label: 'Application settings'
    });

    return states;
}