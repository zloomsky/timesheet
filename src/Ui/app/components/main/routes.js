module.exports = states;

function states() {
    var states = [];
    var state = (name, config) => states.push({ name: name, config: config }); 

    state('main', {
        url: '/',
        template: require('html!components/main/main.html'),
        controller: 'MainController',
        controllerAs: 'vm'       
    });

    return states;
}