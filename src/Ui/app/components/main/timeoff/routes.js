module.exports = states;

function states() {
    var states = [];
    var state = (name, config) => states.push({ name: name, config: config });

    state('main.timeoff', {
        url: 'timeoff',
        views: {
            'content@main': {
                template: require('html!components/main/timeoff/timeoff.html'),
                controller: 'TimeoffController',
                controllerAs: 'vm'
            }
        },
        label: 'Timeoff history'
    });

     state('main.timeoff.edit', {
        url: '/:id',
        views: {
            'content@main': {
                template: require('html!components/main/timeoff/edit/edit.html'),
                controller: 'TimeoffEditController',
                controllerAs: 'vm'
            }          
        },
        label: 'Timeoff edit'
    });

    return states;
}