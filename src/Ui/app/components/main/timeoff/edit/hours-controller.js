module.exports = { name: 'TimeoffHoursController', body: controller };

controller.$inject = ['$http', '$mdDialog', 'vacationTypes', 'state'];
function controller($http, $mdDialog, vacationTypes, state) {
  var vm = this;

  vm.typeAllDays;
  vm.hoursAllDays;
  vm.onTypeAllDaysChange = onTypeAllDaysChange;
  vm.onHoursAllDaysChange = onHoursAllDaysChange;
  vm.state = state;
  vm.total = total;
  vm.vacationTypes = vacationTypes;
  vm.cancel = $mdDialog.cancel;
  vm.submit = submit;

  ////   

  function onHoursAllDaysChange() {
    vm.state
      .filter(d => { var d = new Date(d.Date).getDay(); return d != 0 && d != 6; })
      .forEach(d => vm.hoursAllDays != null ? d.Quantity = vm.hoursAllDays : void (0));
  }

   function onTypeAllDaysChange() {
    vm.state
      .filter(d => { var d = new Date(d.Date).getDay(); return d != 0 && d != 6; })
      .forEach(d => vm.typeAllDays != null ? d.Type = vm.typeAllDays : void (0));
  }

  function total() {
    return vm.state.reduce((s, item) => s + item.Quantity, 0);
  }

  function submit() {
    $mdDialog.hide(vm.state);
  }

};
