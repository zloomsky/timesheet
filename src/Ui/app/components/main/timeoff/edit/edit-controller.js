module.exports = { name: 'TimeoffEditController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http', '$q', '$stateParams'];
function resolve($http, $q, $stateParams) {
  return $q.all([
    $http.get(`api/timeoff/history/details/${$stateParams.id}`).then(r => r.data),
    $http.get('api/wfe/vacationtypes').then(r => r.data)
  ]);
};

controller.$inject = ['$scope', '$state', '$mdDialog', '$http', '$filter', '$mdToast', 'FooterService', 'data'];
function controller($scope, $state, $mdDialog, $http, $filter, $mdToast, FooterService, data) {
  var vm = this;
  vm.data = data[0];
  vm.busy = false;
  vm.remove = remove;
  vm.add = add;
  vm.hours = hours;

  vm.total = total;
  vm.totalWeek = totalWeek;
  vm.detailsWeek = detailsWeek;

  FooterService.enable([
    { Text: "Save", Icon: "save", Id: "save" },
    { Text: "Submit", Icon: "arrow_upward", Id: "submit" },
    { Text: "Cancel", Icon: "cancel", Id: "cancel" }
  ]);
  $scope.$on('footer', handleFooter);
  $scope.$on("$destroy", () => FooterService.disable());


  ///

  function total() {
    var days = [].concat.apply([], vm.data.Weeks.map(w => w.Days));
    var paid = days.filter(d => d.Type === 1).reduce((state, item) => state + item.Quantity, 0) | 0;
    var unpaid = days.filter(d => d.Type == 0).reduce((state, item) => state + item.Quantity, 0) | 0;
    return `${paid + unpaid} (paid: ${paid}/ unpaid: ${unpaid})`;
  }

  function totalWeek(week) {
    return week.Days.reduce((state, item) => state + item.Quantity, 0) | 0;
  }

  function detailsWeek(week) {
    var paid = week.Days.filter(d => d.Type === 1).reduce((state, item) => state + item.Quantity, 0) | 0;
    var unpaid = week.Days.filter(d => d.Type === 0).reduce((state, item) => state + item.Quantity, 0) | 0;
    return `paid: ${paid}, unpaid: ${unpaid}.`;
  }

  function remove(row) {
    vm.data.Weeks.splice(row, 1);
  }

  function sameWeek(a, b) {
    var start = new Date(a.Start).getTime() == new Date(b.Start).getTime();
    var end = new Date(a.End).getTime() == new Date(b.End).getTime();
    return start && end;
  } 

  function excludeUsedWeeks(weeks) {
    return weeks.filter(w => !vm.data.Weeks.find(d => sameWeek(w, d)));
  }

  function add() {
    $mdDialog.show({
      locals: { vacationTypes: data[1] },
      controller: 'TimeoffAddController',
      controllerAs: 'vm',
      template: require('html!./add.html'),
      clickOutsideToClose: true,
      fullscreen: true
    })
    .then(excludeUsedWeeks)
    .then(weeks => vm.data.Weeks.push(...weeks));
  }

  function hours(item) {
    var state = item.Days.map(d => angular.copy(d, {}));
    $mdDialog.show({
      locals: { vacationTypes: data[1], state: state },
      controller: 'TimeoffHoursController',
      controllerAs: 'vm',
      template: require('html!./hours.html'),
      clickOutsideToClose: true,
      fullscreen: true
    }).then(s => item.Days = s);
  }

  function save() {
    if (vm.data.Weeks.length === 0) {
      warn("Timeoff is empty.");
      return;
    }

    var total = [].concat.apply([], vm.data.Weeks.map(w => w.Days))
      .reduce((state, item) => state + item.Quantity, 0) | 0;
    if (!total) {
      warn("Timeoff is empty.");
      return;
    }

    if (vm.data.Status !== 0) {
      warn("Timeoff is submitted.");
      return;
    }

    vm.busy = true;
    var task;
    if ($state.params.id) {
      task = $http.post(`api/timeoff/history/${$state.params.id}`, vm.data);
    } else {
      task = $http.put('api/timeoff/history', vm.data);
    }
    task
      .then(() => $state.go('main.timeoff', {}, { reload: 'main.timeoff' }))
      .finally(() => vm.busy = false);
  }


  function submit() {
    if (!$state.params.id) {
      warn("Timeoff is not saved.");
      return;
    }

    if (vm.data.Status !== 0) {
      warn("Timeoff is submitted.");
      return;
    }

    vm.busy = true;
    $http.post(`api/timeoff/submit/${$state.params.id}`)
      .then(() => $state.go('main.timeoff', {}, { reload: 'main.timeoff' }))
      .finally(() => vm.busy = false);
  }

  function handleFooter(event, args) {
    switch (args) {
      case 'save': save(); break
      case 'submit': submit(); break
      case 'cancel': $state.go('main.timeoff'); break
    };
  }

  function warn(err) {
    if (!err) return;
    $mdToast.show({
      locals: { message: err },
      position: 'bottom right',
      controller: 'ToastController',
      controllerAs: 'vm',
      templateUrl: 'warn-toast',
    })
  }
}