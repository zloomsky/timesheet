module.exports = { name: 'TimeoffAddController', body: controller };

controller.$inject = ['$http', '$mdDialog', '$filter', 'vacationTypes'];
function controller($http, $mdDialog, $filter, vacationTypes) {
  var vm = this;

  let yesterday = new Date(new Date().setDate(new Date().getDate() - 1));
  yesterday.setHours(0, 0, 0, 0);

  let tomorrow = new Date(new Date().setDate(new Date().getDate() + 1));
  tomorrow.setHours(0, 0, 0, 0); 

  vm.hours = null;
  vm.vacationType = vacationTypes[0].value;
  vm.start = new Date(yesterday);
  vm.end = new Date(tomorrow);
  vm.cancel = $mdDialog.cancel;
  vm.vacationTypes = vacationTypes;
  vm.ok = ok;

  ////  


  function ok() {
    var start = getWeekStart(vm.start);
    var end = getWeekEnd(vm.end);

    var days = [];
    var current = new Date(start);
    while (current <= end) {
      var quantity = 0;
      var dayOfWeek = current.getDay();

      if (current >= vm.start && current <= vm.end && dayOfWeek != 6 && dayOfWeek != 0) {
        quantity = vm.hours;
      }

      var day = { Date: new Date(current), Quantity: quantity, Type: vm.vacationType };
      days.push(day);
      current.setDate(current.getDate() + 1);
    }

    var weeks = [];
    while (days.length) {
      var slice = days.splice(0, 7);
      var week = { Days: slice, Start: new Date(slice[0].Date), End: new Date(slice[6].Date) };
      weeks.push(week);
    }

    $mdDialog.hide(weeks);
  }

  function getWeekEnd(date) {
    var end = new Date(date);
    var day = end.getDay();
    var diff = end.getDate() + (6 - (day % 7));
    return new Date(end.setDate(diff)).setHours(0, 0, 0, 0);
  }

  function getWeekStart(date) {
    var start = new Date(date);
    var day = start.getDay();
    var diff = start.getDate() - day;
    return new Date(start.setDate(diff)).setHours(0, 0, 0, 0);
  }

};
