module.exports = { name: 'TimeoffController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http'];
function resolve($http) {
  return $http.get('api/timeoff/history').then(r => r.data);
};

controller.$inject = ['$state', '$http', 'data'];
function controller($state, $http, data) {
  var vm = this;
  vm.items = data.Items;
  vm.total = data.Total;
  vm.getStatus = getStatus;

  vm.page = 1;
  vm.limit = 12;
  vm.pages = genPages(vm.total, vm.limit);
  vm.getPage = getPage;
  vm.getNextPage = getNextPage;
  vm.getPrevPage = getPrevPage;
  vm.canGetNextPage = canGetNextPage;
  vm.canGetPrevPage = canGetPrevPage;

  ////  

  function getStatus(s) {
     switch (s) {
      case 0: return 'In Progress';
      case 1: return 'Submitted';
      case 2: return 'Approved';
      case 3: return 'Rejected';
    };
  }

  function genPages(total, limit) {
    return [...Array(Math.ceil(total / limit))].map((_, i) => ++i);
  }

  function getPage(page) {
    var params = { page: page, limit: vm.limit }
    vm.busy = true;
    $http.get('api/timeoff/history', { params })
      .then(r => {
        vm.items = r.data.Items;
        vm.total = r.data.Total;
        vm.pages = genPages(vm.total, vm.limit);
        vm.busy = false;
        vm.page = page;
      });
  }

  function getNextPage() {
    if (vm.canGetNextPage()) {
      vm.getPage(Number(vm.page) + 1);
    }
  }

  function getPrevPage() {
    if (vm.canGetPrevPage()) {
      vm.getPage(vm.page - 1);
    }
  }

  function canGetNextPage() {
    return vm.page < vm.pages.length;
  }

  function canGetPrevPage() {
    return vm.page > 1;
  }
};
