module.exports = { name: 'MainController', body: controller };

controller.$inject = ['$rootScope', '$scope', '$mdSidenav', '$state', 'UserService', 'FooterService', 'HeaderService'];
function controller($rootScope, $scope, $mdSidenav, $state, UserService, FooterService, HeaderService) {
  var vm = this;
  vm.logout = () => UserService.logout();
  vm.toggleMenu = () => $mdSidenav('left').toggle();
  vm.footerButtons = [];

  vm.search = false;
  vm.headerEnabled = false;
  vm.parameters = [];
  vm.headerParameters = [];


  FooterService.onEnable = (b) => vm.footerButtons = b;
  FooterService.onDisable = () => vm.footerButtons = [];

  HeaderService.onEnable = onEnable;
  HeaderService.onDisable = onDisable;

  $scope.$watch("vm.parameters", (a, b) => (a.length !== b.length) ? $rootScope.$broadcast('header-params', vm.parameters) : void (0));

  ////     

  function onEnable(p) {
    vm.parameters = p.filter(p => p.Active).map(p => p.Id);
    vm.headerParameters = p;
    vm.headerEnabled = true;
  }

  function onDisable() {
    vm.headerParameters = [];
    vm.headerEnabled = false;
    vm.search = false;
  }
};
