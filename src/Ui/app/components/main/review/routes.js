module.exports = states;

function states() {
    var states = [];
    var state = (name, config) => states.push({ name: name, config: config });
  
    state('main.review', {
        url: 'review/:empNo',
        views: {
            'content@main': {
                template: require('html!components/main/review/review.html'),
                controller: 'ReviewController',
                controllerAs: 'vm'
            }            
        },
        label: 'Requests'     
    }); 

    state('main.review.timesheet', {
        url: '/:id/:code',
        views: {
            'content@main': {
                template: require('html!components/main/review/detail/timesheet-detail.html'),
                controller: 'TimesheetDetailController',
                controllerAs: 'vm'
            }                   
        },
        label: 'Timesheet review'        
    }); 

     state('main.review.timeoff', {
        url: '/:id',
        views: {
            'content@main': {
                template: require('html!components/main/review/detail/timeoff-detail.html'),
                controller: 'TimeoffDetailController',
                controllerAs: 'vm'
            }                   
        },
        label: 'Timeoff review'        
    }); 

    return states;
}