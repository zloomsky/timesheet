module.exports = { name: 'ReviewController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http', '$stateParams'];
function resolve($http, $stateParams) {
    return $http.get(`api/employee/requests/${$stateParams.empNo}`).then(r => r.data);
};

controller.$inject = ['$scope', '$state', '$mdDialog', '$http', 'data', 'HeaderService'];
function controller($scope, $state, $mdDialog, $http, data, HeaderService) {
    var vm = this;

    vm.busy = false;
    vm.items = data.Items;
    vm.total = data.Total;
    vm.getStatus = getStatus;
    vm.getType = getType;
    vm.goDetail = goDetail;

    vm.page = 1;
    vm.limit = 12;
    vm.pages = genPages(vm.total, vm.limit);
    vm.getPage = getPage;
    vm.getNextPage = getNextPage;
    vm.getPrevPage = getPrevPage;
    vm.canGetNextPage = canGetNextPage;
    vm.canGetPrevPage = canGetPrevPage;
   
    vm.itemType = 0;
    vm.history = 0;

    HeaderService.enable([
        {Text: "Timesheet", Id: "timesheet", Active: true },
        {Text: "Timeoff", Id: "timeoff", Active: true },
        {Text: "Only new", Id: "onlynew", Active: true }
    ]);

    $scope.$on('header-params', handleHeader);   
    $scope.$on("$destroy", () => HeaderService.disable());

    ////  

    function handleHeader(event, args) {
        var onlynew = args.indexOf("onlynew") > -1;
        var timeoff = args.indexOf("timeoff") > -1;
        var timesheet = args.indexOf("timesheet") > -1;
        
        var itemType = vm.itemType;
        var history = vm.history;
       
        if (timesheet && !timeoff){
            itemType = 1;
        } 
        if (!timesheet && timeoff) {
            itemType = 2;
        }

        history = onlynew? 0 : 1;

        if (vm.itemType !== itemType || vm.history !== history) {
            vm.itemType = itemType;
            vm.history = history;
            getPage(1);
        }        
    }

    function genPages(total, limit) {
        return [...Array(Math.ceil(total / limit))].map((_, i) => ++i);
    }

    function getPage(page) {
        var params = { t: vm.itemType, h: vm.history, page: page, limit: vm.limit }
        vm.busy = true;
        $http.get(`api/employee/requests/${$state.params.empNo}`, { params })
            .then(r => {
                vm.items = r.data.Items;
                vm.total = r.data.Total;
                vm.pages = genPages(vm.total, vm.limit);
                vm.busy = false;
                vm.page = page;
            });
    }

    function getNextPage() {
        if (vm.canGetNextPage()) {
            vm.getPage(Number(vm.page) + 1);
        }
    }

    function getPrevPage() {
        if (vm.canGetPrevPage()) {
            vm.getPage(vm.page - 1);
        }
    }

    function canGetNextPage() {
        return vm.page < vm.pages.length;
    }

    function canGetPrevPage() {
        return vm.page > 1;
    }

    function getStatus(s) {
        switch (s) {
            case 0: return 'In Progress';
            case 1: return 'Submitted';
            case 2: return 'Approved';
            case 3: return 'Rejected';
        };
    }

    function getType(s) {
        switch (s) {
            case 1: return 'Timesheet';
            case 2: return 'Timeoff';            
        };
    }

    function goDetail(item) {
        switch (item.Type) {
            case 1: $state.go('main.review.timesheet', {code: item.Period.Code, id: item.Id}); break;
            case 2: $state.go('main.review.timeoff', {id: item.Id}); break;           
        };
    }
};
