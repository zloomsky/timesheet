module.exports = { name: 'TimesheetDetailController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http', '$stateParams'];
function resolve($http, $stateParams) {
    return $http.get(`api/timesheet/history/${$stateParams.code}/${$stateParams.id}`)
        .then(r => r.data);
};

controller.$inject = ['$scope', '$state', '$mdDialog', '$http', '$mdToast', 'data', 'FooterService'];
function controller($scope, $state, $mdDialog, $http, $mdToast, data, FooterService) {
    var vm = this;

    vm.data = data;
    vm.description = description;
    vm.hoursTotal = hoursTotal;

    FooterService.enable([
        { Text: "Approve", Icon: "thumb_up", Id: "approve" },
        { Text: "Reject", Icon: "thumb_down", Id: "reject" },
        { Text: "Cancel", Icon: "cancel", Id: "cancel" }
    ]);
    $scope.$on('footer', handleFooter);
    $scope.$on("$destroy", () => FooterService.disable());


    function description(entry) {
        return [entry.PayrollCode, entry.LaborCode, entry.Order, entry.Job, entry.JobTask, entry.WorkArea].filter(i => i).join(' - ');
    }

    function hoursTotal(entry) {
        return entry.Days.reduce((state, item) => state + item.Quantity, 0);
    }

    function handleFooter(event, args) {
        switch (args) {
            case 'approve': approve(); break
            case 'reject': reject(); break
            case 'cancel': $state.go('main.review'); break
        };
    }

    function approve() {
        if (vm.data.Status !== 1) {
            warn("Timesheet is processed.");
            return;
        }

        vm.busy = true;
        $http.post(`api/timesheet/approve/${$state.params.id}`)
            .then(() => $state.go('main.review', {}, { reload: 'main.review' }))
            .finally(() => vm.busy = false);
    }

    function reject() {
        if (vm.data.Status !== 1) {
            warn("Timesheet is processed.");
            return;
        }

        vm.busy = true;
        $http.post(`api/timesheet/reject/${$state.params.id}`)
            .then(() => $state.go('main.review', {}, { reload: 'main.review' }))
            .finally(() => vm.busy = false);
    }

    function warn(err) {
        if (!err) return;
        $mdToast.show({
            locals: { message: err },
            position: 'bottom right',
            controller: 'ToastController',
            controllerAs: 'vm',
            templateUrl: 'warn-toast',
        })
    }

};
