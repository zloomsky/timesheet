module.exports = { name: 'TimeoffDetailController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http', '$q', '$stateParams'];
function resolve($http, $q, $stateParams) {
    return $http.get(`api/timeoff/history/details/${$stateParams.id}`).then(r => r.data);
};

controller.$inject = ['$scope', '$state', '$mdDialog', '$http', '$mdToast', 'data', 'FooterService'];
function controller($scope, $state, $mdDialog, $http, $mdToast, data, FooterService) {
    var vm = this;

    vm.data = data;
    vm.totalWeek = totalWeek;
    vm.total = total;
    vm.detailsWeek = detailsWeek;

    FooterService.enable([
        { Text: "Approve", Icon: "thumb_up", Id: "approve" },
        { Text: "Reject", Icon: "thumb_down", Id: "reject" },
        { Text: "Cancel", Icon: "cancel", Id: "cancel" }
    ]);
    $scope.$on('footer', handleFooter);
    $scope.$on("$destroy", () => FooterService.disable());


    function handleFooter(event, args) {
        switch (args) {
            case 'approve': approve(); break
            case 'reject': reject(); break
            case 'cancel': $state.go('main.review'); break
        };
    }

    function total() {
        var days = [].concat.apply([], vm.data.Weeks.map(w => w.Days));
        var paid = days.filter(d => d.Type === 1).reduce((state, item) => state + item.Quantity, 0) | 0;
        var unpaid = days.filter(d => d.Type == 0).reduce((state, item) => state + item.Quantity, 0) | 0;
        return `${paid + unpaid} (paid: ${paid}/ unpaid: ${unpaid})`;
    }

    function totalWeek(week) {
        return week.Days.reduce((state, item) => state + item.Quantity, 0) | 0;
    }

    function detailsWeek(week) {
        var paid = week.Days.filter(d => d.Type === 1).reduce((state, item) => state + item.Quantity, 0) | 0;
        var unpaid = week.Days.filter(d => d.Type === 0).reduce((state, item) => state + item.Quantity, 0) | 0;
        return `paid: ${paid}, unpaid: ${unpaid}.`;
    }

    function approve() {
        if (vm.data.Status !== 1) {
            warn("Timeoff is processed.");
            return;
        }

        vm.busy = true;
        $http.post(`api/timeoff/approve/${$state.params.id}`)
            .then(() => $state.go('main.review', {}, { reload: 'main.review' }))
            .finally(() => vm.busy = false);
    }

    function reject() {
        if (vm.data.Status !== 1) {
            warn("Timeoff is processed.");
            return;
        }

        vm.busy = true;
        $http.post(`api/timeoff/reject/${$state.params.id}`)
            .then(() => $state.go('main.review', {}, { reload: 'main.review' }))
            .finally(() => vm.busy = false);
    }

    function warn(err) {
        if (!err) return;
        $mdToast.show({
            locals: { message: err },
            position: 'bottom right',
            controller: 'ToastController',
            controllerAs: 'vm',
            templateUrl: 'warn-toast',
        })
    }

};
