module.exports = { name: 'TimesheetSettingsController', body: controller };

controller.$inject = ['$http', '$mdDialog', '$q', 'data', 'state'];
function controller($http, $mdDialog, $q, data, state) {
  var vm = this;

  vm.payrollCodes = data[0];
  vm.orders = data[1];
  vm.jobs = data[2];
  vm.laborCodes = data[3];
  vm.workAreas = data[4];
  vm.jobTasks = data[5];
  vm.periodStart = new Date(new Date(data[6].Start).setHours(0, 0, 0, 0));
  vm.periodEnd = new Date(new Date(data[6].End).setHours(0, 0, 0, 0));
  vm.daysStart = vm.periodStart;
  vm.daysEnd = vm.periodEnd;
  vm.hours;

  vm.payrollCode = vm.payrollCodes.find(p => p.Code == state.PayrollCode);
  vm.state = state;

  vm.onlyPayrollDates = onlyPayrollDates;
  vm.onHoursChange = onHoursChange;
  vm.loadJobTasks = loadJobTasks;
  vm.onPayrollCodeChange = onPayrollCodeChange;
  vm.onJobChange = onJobChange;
  vm.onServiceOrderChange = onServiceOrderChange;
  vm.cancel = $mdDialog.cancel;
  vm.ok = ok;
  vm.goHours = goHours;

  ////  

  function isWorkDayInRange(date) {
    let day = new Date(date);
    let dayn = new Date(date).getDay();
    return day >= vm.daysStart & day <= vm.daysEnd & dayn != 6 & dayn != 0;
  }

  function onHoursChange() {
    vm.state.Days
      .filter(d => isWorkDayInRange(d.Date))
      .forEach(d => vm.hours != null ? d.Quantity = vm.hours : void (0));
  }

  function onlyPayrollDates(date) {
    return date >= vm.periodStart && date <= vm.periodEnd;
  };


  function loadJobTasks() {
    return $http
      .get(`api/wfe/jobs/${vm.state.Job}`)
      .then(r => vm.jobTasks = r.data);
  }


  function onJobChange() {
    vm.state.JobTask = null;
    vm.state.Order = null;
  }

  function onPayrollCodeChange() {
    if (!vm.payrollCode.Payable) {
      vm.state.Job = null;
      vm.state.JobTask = null;
      vm.state.Order = null;
    }
  }

  function onServiceOrderChange() {
    vm.state.Job = null;
    vm.state.JobTask = null
  }

  function ok() {
    vm.state.PayrollCode = vm.payrollCode.Code;
    $mdDialog.hide({ state: vm.state, goHours: false });
  }

  function goHours() {
    vm.state.PayrollCode = vm.payrollCode.Code;
    $mdDialog.hide({ state: vm.state, goHours: true });
  }

};
