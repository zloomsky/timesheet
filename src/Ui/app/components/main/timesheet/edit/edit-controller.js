module.exports = { name: 'TimesheetEditController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http', '$stateParams'];
function resolve($http, $stateParams) {
  var getDays = period => $http.get(`api/timesheet/days`, { params: { start: period.Start, end: period.End } });
  var data;
  return $http.get(`api/timesheet/history/${$stateParams.code}/${$stateParams.id}`)
    .then(r => { data = r.data; return getDays(r.data.Period); })
    .then(r => [data, r.data]);
}

controller.$inject = ['$state', '$http', '$mdDialog', '$q', '$scope', '$mdToast', 'FooterService', 'data'];
function controller($state, $http, $mdDialog, $q, $scope, $mdToast, FooterService, data) {
  var vm = this;
  var defRow = data[1];

  vm.data = data[0];  
  vm.description = description;
  vm.hoursTotal = hoursTotal;
  vm.payrollTotal = payrollTotal;

  vm.add = add;
  vm.remove = remove;
  vm.settings = settings;
  vm.hours = hours;

  FooterService.enable([
    {Text: "Save", Icon: "save", Id: "save" },
    {Text: "Submit", Icon: "arrow_upward", Id: "submit" },
    {Text: "Cancel", Icon: "cancel", Id: "cancel" }
  ]);
  $scope.$on('footer', handleFooter);
  $scope.$on("$destroy", () => FooterService.disable());

  ////  

  function description(values) {
    return values.filter(i => i).join(' - ');
  }

  function hoursTotal(entry) {
    return entry.Days.reduce((state, item) => state + item.Quantity, 0);
  }

  function payrollTotal() {
    return vm.data.Days.reduce((state, item) => state + item.Days.reduce((s, i) => s + i.Quantity, 0), 0);
  }

  function remove(row) {
    vm.data.Days.splice(row, 1);
  }

  function handleFooter(event, args) {   
    switch (args) {
      case 'save': save(); break
      case 'submit': submit(); break
      case 'cancel': $state.go('main.timesheet'); break   
    };
  }

  function add() {
    var newItem = angular.copy(defRow, {});
    editor(newItem, -1)
      .then(r => { vm.data.Days.push(r.state); return r;}, err => warn(err))
      .then(r => r.goHours? hours(r.state): void(0));
  }

  function settings(item) {
    var state = angular.copy(item, {});
    editor(state, vm.data.Days.indexOf(item))
      .then(r => { angular.merge(item, r.state); return r.goHours;}, err => warn(err))
      .then(goHours => goHours? hours(item): void(0));
  }

  function hours(item) {
    var state = item.Days.map(d => angular.copy(d, {}));
    $mdDialog.show({
      locals: { state },
      controller: 'TimesheetHoursController',
      controllerAs: 'vm',
      template: require('html!./hours.html'),
      clickOutsideToClose: true,
      fullscreen: true
    }).then(s => item.Days = s);
  }

  function save() {    
    if (vm.data.Days.length === 0){
      warn("Timesheet is empty.");
      return;
    }

    if (vm.data.Status !== 0){
      warn("Timesheet is submitted.");
      return;
    }

    vm.busy = true;
    var task;
    if ($state.params.id) {
      task = $http.post(`api/timesheet/history/${$state.params.code}/${$state.params.id}`, vm.data);
    } else {
      task = $http.put(`api/timesheet/history/${$state.params.code}`, vm.data);
    }
    task
      .then(() => $state.go('main.timesheet', {}, { reload: 'main.timesheet' }))
      .finally(() => vm.busy = false);
  }

  function submit() {
    if(!$state.params.id) {
      warn("Timesheet is not saved.");
      return;
    }

    if(vm.data.Status !== 0) {
      warn("Timesheet is submitted.");
      return;
    }

    vm.busy = true;
    $http.post(`api/timesheet/submit/${$state.params.id}`)
      .then(() => $state.go('main.timesheet', {}, { reload: 'main.timesheet' }))
      .finally(() => vm.busy = false);
  }

  function editor(state, itemIndex) {
    var jobTasks = state => state.Job ? $http.get(`api/wfe/jobs/${state.Job}`).then(r => r.data) : $q.when([]);
    var addDefaultLaborCode = codes => {codes.some(c => c.Value == defRow.LaborCode)? void(0) : codes.push({Value: defRow.LaborCode, Text: ''}); return codes}; 
    
    return $q.all([
      $http.get('api/wfe/payrollcodes').then(r => r.data),
      $http.get('api/wfe/orders').then(r => r.data),
      $http.get('api/wfe/jobs').then(r => r.data),
      $http.get('api/wfe/laborcodes').then(r => r.data).then(addDefaultLaborCode),
      $http.get('api/wfe/workareas').then(r => r.data),
      jobTasks(state),
      $q.when({Start: vm.data.Period.Start, End: vm.data.Period.End})
    ]).then(data =>
      $mdDialog.show({
        locals: { data, state },
        controller: 'TimesheetSettingsController',
        controllerAs: 'vm',
        template: require('html!./settings.html'),
        clickOutsideToClose: true,
        fullscreen: true
      }))
      .then(r => checkDuplicates(r.state, itemIndex)? $q.when(r) : $q.reject('Timesheet entry with same parameters already exist.'));
  }

  function checkDuplicates(item, itemIndex) {
    var duplicates = vm.data.Days.filter((d, i) => i !== itemIndex).filter(d => daysIsEqual(d, item)).length;
    return duplicates < 1;
  }

  function daysIsEqual(a, b) {
    return a.PayrollCode == b.PayrollCode &&
      a.LaborCode == b.LaborCode &&
      a.WorkArea == b.WorkArea &&
      a.Order == b.Order &&
      a.Job == b.Job &&
      a.JobTask == b.JobTask;
  }

  function warn(err) {
    if (!err) return;
    $mdToast.show({
      locals: { message: err },
      position: 'bottom right',
      controller: 'ToastController',
      controllerAs: 'vm',
      templateUrl: 'warn-toast',
    })
  }
};