module.exports = { name: 'TimesheetHoursController', body: controller };

controller.$inject = ['$http', '$mdDialog', 'state'];
function controller($http, $mdDialog, state) {
  var vm = this;

  vm.allWorkDays = null;
  vm.state = state;
  vm.total = total;
  vm.cancel = $mdDialog.cancel;
  vm.submit = submit;
  vm.onAllWorkDaysChange = onAllWorkDaysChange;

  ////   

  function total() {
    return vm.state.reduce((s, item) => s + item.Quantity, 0);
  }

  function submit() {
    $mdDialog.hide(vm.state);
  }

  function onAllWorkDaysChange() {
    vm.state
      .filter(d => { var d = new Date(d.Date).getDay(); return d != 6 && d != 0; })
      .forEach(d => vm.allWorkDays != null ? d.Quantity = vm.allWorkDays : void (0));
  }

};
