module.exports = states;

function states() {
    var states = [];
    var state = (name, config) => states.push({ name: name, config: config });

    state('main.timesheet', {
        url: 'timesheet',
        views: {
            'content@main': {
                template: require('html!components/main/timesheet/timesheet.html'),
                controller: 'TimesheetController',
                controllerAs: 'vm'
            }
        },
        label: 'Timesheet history'
    });

    state('main.timesheet.edit', {
        url: '/:code/:id',
        views: {
            'content@main': {
                template: require('html!components/main/timesheet/edit/edit.html'),
                controller: 'TimesheetEditController',
                controllerAs: 'vm'
            }        
        },
        label: 'Timesheet edit'
    });

    return states;
}