module.exports = { name: 'TimesheetAddDialogController', body: controller };

controller.$inject = ['$http', '$mdDialog', 'data'];
function controller($http, $mdDialog, data) {
    var vm = this;

    vm.payrollPeriod = data[0].Code;
    vm.payrollPeriods = data;
    vm.cancel = $mdDialog.cancel;
    vm.ok = ok;

    ////

    function ok() {
        $mdDialog.hide(vm.payrollPeriod);
    }

};
