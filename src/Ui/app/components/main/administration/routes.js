module.exports = states;

function states() {
    var states = [];
    var state = (name, config) => states.push({ name: name, config: config });

    state('main.administration', {
        url: 'administration/:id',
        views: {
            'content@main': {
                template: require('html!components/main/administration/administration.html'),
                controller: 'AdministrationController',
                controllerAs: 'vm'
            }
        },
        label: 'My team'
    });  

    return states;
}