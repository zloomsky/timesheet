module.exports = { name: 'AdministrationController', body: controller, provider: { data: resolve } };

resolve.$inject = ['$http', '$stateParams'];
function resolve($http, $stateParams) {
  return $http.get(`api/employee/subordinates/${$stateParams.id}`).then(r => r.data)
};

controller.$inject = ['$state', '$http', 'data'];
function controller($state, $http, data) {
  var vm = this;

  vm.layer = data;

  ////

};
