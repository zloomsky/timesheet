module.exports = loadTemplates;  

loadTemplates.$inject = ['$templateCache'];
function loadTemplates($templateCache) {
	$templateCache.put('error-toast', require('html!./error-toast.html'));
	$templateCache.put('warn-toast', require('html!./warn-toast.html'));
	$templateCache.put('save-submit-cancel-footer', require('html!./save-submit-cancel-footer.html'));
	$templateCache.put('approve-reject-cancel-footer', require('html!./approve-reject-cancel-footer.html'));
}