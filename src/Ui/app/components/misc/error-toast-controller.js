module.exports = {name: 'ToastController', body: controller};

controller.$inject = ['$mdToast', 'message'];
function controller($mdToast, message) {
	var vm = this;   
	vm.message = message;
	vm.close = () => $mdToast.hide();	
}


