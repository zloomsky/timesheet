module.exports = states;

function states() {
    var states = [];
    var state = (name, config) => states.push({ name: name, config: config });

    state('login', {
        url: '/login',
        template: require('html!components/login/login.html'),
        controller: 'LoginController',
        controllerAs: 'vm'
    }); 

    return states;
}