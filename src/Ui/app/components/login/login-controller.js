module.exports = {name: 'LoginController', body: controller};

controller.$inject = ['$state', 'UserService'];
function controller($state, UserService) {

	var vm = this;   
	vm.credentials = {login : '', password: '', persistent: false};     
	vm.submit = submit;  
	vm.authorized = authorized;
	vm.busy = false;	
    ////////////  

	function authorized(state){
		vm.form.login.$setValidity('authorized', state);
	}

    function submit() { 
    	vm.busy = true;
    	UserService.login(vm.credentials)
    	.then(() => $state.go('main.timesheet'), () => authorized(false))      
    	.finally(() => vm.busy = false);
    }   
}


