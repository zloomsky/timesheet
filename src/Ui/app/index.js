'use strict';

require('./index.scss');

require('angular');
require('angular-sanitize');
require('angular-animate');
require('angular-ui-router');
require('angular-loading-bar');
require('angular-material');
require('angular-messages');

var kernel = angular.module('app', ['ui.router', 'ngAnimate', 'angular-loading-bar', 'ngMaterial', 'ngMessages']);

//register controllers
var controllers = require.context('./components', true, /controller.js$/);
controllers.keys().forEach(function(key){
	var definition = controllers(key);	
	kernel.controller(definition.name, definition.body);
	if (definition.provider)	
		kernel.provider(definition.name, function(){ this.$get = definition.provider });	
});

//register services
var services = require.context('./services', true, /service.js$/);
services.keys().forEach(function(key){
	var definition = services(key)();	
	kernel.service(definition.name, definition.body);	
});

//apply configs
var configs = require.context('./configs', true, /config.js$/);
configs.keys().forEach(function(key){
	kernel.config(configs(key));	
});

//put templates
kernel.run(require('./components/misc/templates.js'));

//run
run.$inject = ['$rootScope', '$state', '$animate'];
function run($rootScope, $state, $animate) {
	$rootScope.currentUser = null;	
	$rootScope.$state = $state;	
}
kernel.run(run);
