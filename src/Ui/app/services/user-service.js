module.exports = function () { return { name: 'UserService', body: service } };

service.$inject = ['$rootScope', '$q', '$state', '$http'];
function service($rootScope, $q, $state, $http) {
  var service = this;
  service.logout = logout;
  service.login = login;
  service.getCurrentUser = getCurrentUser;

  ///////////////        

  function logout() {
    return $http.delete('api/login')
      .then(() => {
        $rootScope.currentUser = null; 
        $state.go('login');
      });      
  }

  function login(credentials) {
    return $http.post('api/login', credentials)
      .then(r => $rootScope.currentUser = r.data)
      .then(r => r.data);
  }

  function getCurrentUser() {
    if ($rootScope.currentUser) {
      return $q.when($rootScope.currentUser);
    }
    return $http.get('api/login')
      .then(r => $rootScope.currentUser = r.data)
      .then(r => r.data);
  }
}
