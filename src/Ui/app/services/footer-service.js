module.exports = function () { return { name: 'FooterService', body: service } };

function service() {
    var service = this;   

    service.onEnable = null;
    service.onDisable = null;
    service.enable = (b) => { if (service.onEnable) { service.onEnable(b); } };
    service.disable = () => { if (service.onDisable) { service.onDisable(); } };

    ///////////////        

}
