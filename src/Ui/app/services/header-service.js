module.exports = function () { return { name: 'HeaderService', body: service } };

function service() {
  var service = this;

  service.onEnable = null;
  service.onDisable = null;
  service.enable = (p) => { if (service.onEnable) { service.onEnable(p); } };
  service.disable = () => { if (service.onDisable) { service.onDisable(); } };

  ///////////////        

 
}
